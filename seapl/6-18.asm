.286
.model small
.stack 100h

.data
	from db 51 dup('|'), 13, 10, '$'
	to db 38 dup('-'), 13, 10, '$'
	
	sop_string db 10,13,"Write string (max lenght == 100): $"
	buf db 100, 103 dup('$')
	
	heap db 100, 103 dup('$')
	sop_find db 10,13,"Write string to find (max lenght == 20): $"
	token db 20, 23 dup('$')

	not_contains db 13,10,"String not contains substring",13,10,'$'
	contains db 13,10,"String contains substring",13,10,'$'

	new_line db 13,10,'$'
	sop_empty db 13,10,"Empty string",13,10,'$'
	
.code
Start:	
	mov ax, @data
	mov ds, ax
	mov es, ax		
        cld
;;; first
	lea dx, from
	call print_s

	lea dx, to
	call print_s
	
	lea si, from + 1
	lea di, to + 4
	mov cx, 8
	rep movsb

	lea dx, to
	call print_s
	
;;; second
	lea dx, sop_string
	call print_s

	lea dx, buf
	call fgets
	
	cmp byte ptr buf[1], 0
	jne not_empty
	lea dx, sop_empty
	call print_s
	jmp third

not_empty:	
	lea dx, new_line
	call print_s

	call getch_filter
	mov dl, al
	call Print_c

	lea dx, new_line
	call print_s
	
	lea di, buf[2]
	xor ch, ch
	mov cl, buf[1]
	;; symbol is already in al
	xor bx, bx

iter:	
	repne scasb
        jne not_found
        inc bx
not_found:
	jcxz breaak
	jmp iter
breaak:	
	mov ax, bx
	call print_in_system

	lea dx, new_line
	call print_s
	
;;; third
third:	
	lea dx, sop_string
	call print_s

	lea dx, heap
	call fgets

	cmp byte ptr heap[1], 0
	je empty
 
	lea dx, sop_find
	call print_s

	lea dx, token
	call fgets

	cmp byte ptr token[1], 0
	je empty
 
	lea di, heap[2]
	mov al, token[2]
	mov cl, heap[1]
	xor ch, ch
search_loop:	
	repne scasb
	jne break
	call is_equals
	jmp search_loop	
break:	
	lea dx, not_contains
	call print_s
	mov ah, 4ch
	int 21h

empty:
	lea dx, sop_empty
	call print_s
	mov ah, 4ch
	int 21h


is_equals proc
	;; di contains start address + 1
	pusha
	dec di
	lea si, token[2]

	xor ch, ch
	mov cl, token[1]

	repe cmpsb
	je equal

	popa
	ret

equal:	
	popa
	pop cx

	lea dx, contains
	call print_s
	mov ah, 4Ch
	int 21h
endp

fgets 	proc
	mov ah, 0ah
	int 21h
	ret
endp
	
print_s proc
	mov ah, 09h
	int 21h
	ret
endp

getch_filter proc
	mov ah, 08h
	int 21h
	ret
endp


print_in_system proc
	xor cx, cx
	xor bh, bh
	mov bl, 10		
pushing:
	xor dx, dx
	div bx

	push dx
	inc cx

	test ax, ax
jnz pushing

	mov ah, 02h
poping:
	pop dx

	cmp bx, 10
	jl $+10			; to 'add dl, 7'
	cmp dl, 10
	jl notABCDEF
	add dl, 7
notABCDEF:	
	add dl, '0'
	int 21h
loop poping
	ret
endp

print_c	proc
	mov ah, 02h 
	int 21h
	ret
endp

	
end start

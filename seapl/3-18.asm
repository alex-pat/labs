.286
.model small
.stack 100h

.data

	string db 100, 103 dup('$')
	before_string db 20, 23 dup('$')
	after_string db 20, 23 dup('$')

	;; SOP strings
	sop_string db "Write string (max lenght == 100): $"
	sop_before db 10,13,"Write string to replace (max lenght == 20): $"
	sop_after db 10,13,"Write replacing string (max lenght == 20): $"
	sop_result db 10,13,"Result: $"

	sop_error db 10,13,"Too big strings!",10,13,"$"
	
	
.code
Start:	
	mov ax, @data
	mov ds, ax
	mov es, ax		
	
;;; Input 
	lea dx, sop_string
	call print_s
	
	lea dx, string
	call fgets
	
	lea dx, sop_before
	call print_s

	lea dx, before_string
	call fgets
	
	lea dx, sop_after
	call print_s

	lea dx, after_string
	call fgets

	cmp string[1], 0
	je break
	cmp before_string[1], 0
	je break
		
;;; Processing	
	lea di, string[2]
	lea si, before_string[2]
main_loop:
	cmpsb
	jne $+15
	call is_equals
	xor ah, ah
	mov al, string[1]
	cmp ax, 0064h
	jg bad_input
	dec si
	cmp byte ptr [di], '$'
	jz break
jmp main_loop

	
;;; Output
break:	lea dx, sop_result
	call print_s
	lea dx, string[2]
	call print_s

exit:	mov ah, 4Ch
	int 21h

bad_input:
	lea dx, sop_error
	call print_s
	jmp exit

is_equals proc
	;; di contains start address + 1
	;; si contains source address + 1 (before_string[2])
	dec di
	dec si
	pusha

	xor ch, ch
	mov cl, before_string[1]

	repe cmpsb
	jne not_equ

	popa
	mov al, before_string[1] 
	mov cl, after_string[1]
	cmp al, cl
jg greater
jl less
	;; then equals
	;; after popa di will contain insert address
	call insert
	xor bh, bh
	mov bl, after_string[1]
	add di, bx
	inc si
	ret

greater:
	xor dh, dh
	mov dl, after_string[1]
	add dx, di

	xor bh, bh
	mov bl, before_string[1]
	add bx, di
	
	call l_shift
	call insert
	xor bh, bh
	mov bl, after_string[1]
	add di, bx
	inc si
	ret

less:
	xor ah, ah
	;; mov al, before_string[1] 
	;; commeted because al already contains before_string[1]
	add ax, di

	xor dh, dh
	mov dl, cl 		; len(after)
	sub dl, before_string[1]

	call r_shift
	call insert
	xor bh, bh
	mov bl, after_string[1]
	add di, bx
	inc si
	ret
	
not_equ:
	popa
	inc di
	inc si
	ret
endp

	
	
insert 	proc
	;; di contains insert address
	pusha
	lea si, after_string[2]
	xor cx, cx
	add cl, after_string[1]
	rep movsb
	popa
	ret
endp


r_shift proc
	;; ax contains first byte address 
	;; dx contains shift lenght
	pusha
	lea si, string
	mov di, word ptr string[1]
	and di, 00FFh
	add si, di
	add si, 3

	mov di, si
	add di, dx

	mov bx, di
	sub bx, si
	add string[1], bl
	
	mov cx, si
	sub cx, ax
	add cx, 1
	
	std
	rep movsb
	cld
	
	popa
	ret
endp
	
	
l_shift	proc
	;; bx contains start address
	;; dx contains new address
	pusha
	mov si, bx
	mov di, dx
	xor cx, cx
	mov cl, string[1]
	inc cl
	add cx, offset string[2]
	sub cx, bx
	inc cx
	rep movsb
	sub bx, dx
	sub string[1], bl
	popa
	ret
endp
	
	
fgets 	proc
	mov ah, 0ah
	int 21h
	ret
endp
	
print_s proc
	mov ah, 09h
	int 21h
	ret
endp
	
end start

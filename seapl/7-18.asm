.286
.model small
.stack 100h

.data

	argv db ?,?
	string1 db 100, 103 dup('$')
	string2 db 100, 103 dup('$')
	string3 db 100, 103 dup('$')
	sop_string db 10,13,"Write string (max lenght == 100): $"
	new_line db 10,13,'$'

.code
start:
	mov ax, @data
	mov ds, ax
;;; first
	mov cx, 5
first_loop:
	mov ah, 01h
	int 21h
	call far ptr fun1
loop first_loop

;;; second 
	lea dx, new_line
	mov ah, 09h
	int 21h

	mov byte ptr argv[0], 42
	mov byte ptr argv[1], 139
	call fun2

;;; third
	mov cx, 3
	lea dx, string1
third_loop:
	mov bx, dx
	lea dx, sop_string
	mov ah, 09h
	int 21h
	
	mov dx, bx
	mov ah, 0ah
	int 21h

	add dx, 2
	push dx
	add dx, 102
loop third_loop
	call far ptr fun3

	mov ah, 4ch
	int 21h

	
fun1 proc far
	;; one parameter in al
	mov dl, al
	mov ah, 02h 
	int 21h
	ret far	
endp


fun2 proc
	;; two parameters in memory
	xor ah, ah
	mov al, argv[0]

	mov si, 2
print:
	dec si
	xor cx, cx
	mov bx, 10		
pushing:
	xor dx, dx
	div bx

	push dx
	inc cx

	test ax, ax
jnz pushing

	mov ah, 02h
poping:
	pop dx

	add dl, '0'
	int 21h
loop poping

	mov dl, ' '
	mov ah, 02h
	int 21h
	xor ah, ah
	mov al, argv[1]
	cmp si, 0
	jg print
	
	ret
endp
	

fun3 proc far
	;; three parameters in stack
	mov ah, 09h
	
	push bp
	mov bp, sp
	
	lea dx, new_line
	int 21h
	
	mov dx, [bp + 10]
	int 21h

	lea dx, new_line
	int 21h
	
	mov dx, [bp + 8]
	int 21h
	
	lea dx, new_line
	int 21h
	
	mov dx, [bp + 6]
 	int 21h

	pop bp
	retf 6	
endp

end start

	

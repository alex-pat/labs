.286
.model small
.stack 100h

.data

	path_file_buf   db 20, 22 dup(?)
	path_tmp    db   'tmp.txt', 0
	handle  dw ?
	handle_tmp dw ?
	error_msg   db 10,13,"Error!",10,13,'$'
	sop_line  db 10,13,"Write line number: $"
	sop_file  db 10,13,"Write file name: $"

	linum_buf db 2, 4 dup(?)
	linum   db ?
	counter db ?
	char    db 1
.code
open_error:
	lea dx, error_msg
	mov ah, 09h
	int 21h
	mov ah, 4ch
	int 21h
	
start:	mov ax, @data
	mov ds, ax
	mov es, ax
	
	lea dx, sop_file
	call print_s

	lea dx, path_file_buf
	call fgets

	mov di, dx
	inc di
	
	cmp byte ptr [di], 0
	je open_error

	mov dx, di
	inc dx
	add dl, [di]
	mov di, dx
	mov byte ptr [di], 0
	
	lea dx, sop_line
	call print_s

	lea dx, linum_buf
	call fgets
	call num_to_bin

	cmp linum, 0
	jle open_error
	
	mov  ah, 3dh
	mov  al, 0  
	lea  dx, path_file_buf[2]
	int  21h          
	jc   open_error   
	mov  handle, ax   

	mov  ah, 5Bh
	mov  cx, 0
	lea  dx, path_tmp 
	int  21h          
	jc   open_error	  
	mov  handle_tmp, ax

	mov al, byte ptr linum
	dec al
	mov byte ptr counter, al
	jmp reading
enter_checking:
	cmp char, 10
	jne writing
	mov al, counter
	cmp al, 0
	jne non_zero
	mov al, linum
non_zero:
	dec al
	mov counter, al

writing:
	cmp byte ptr counter, 0
	je reading

	mov ah, 40h
	mov bx, handle_tmp
	int 21h
	
reading:
	;; reading byte
	mov ah, 3fh
	mov bx, handle
	mov cx, 1
	lea dx, char
	int 21h
	jc error
	cmp ax, 0
	jne enter_checking
breaak:	

	mov ah, 40h
	lea bx, handle_tmp
	int 21h
	
	;; close
	mov ah, 3eh
	mov bx, handle
	int 21h
	jc error
	
	mov bx, handle_tmp
	int 21h
	jc error
	
	;; delete old
	mov ah, 41h
	lea dx, path_file_buf[2]
	int 21h
	jc error

	;; rename
	mov ah, 56h
	lea dx, path_tmp
	lea di, path_file_buf[2]
	int 21h
	jc error
return:	
	mov ah, 4ch
	int 21h

error:
	lea dx, error_msg
	call print_s
	jmp return


	

fgets 	proc
	mov ah, 0ah
	int 21h
	ret
endp

	
print_s proc
	mov ah, 09h
	int 21h
	ret
endp


num_to_bin proc
	;; dx contains buffer
	xor ax, ax		;contains result number
	lea di, linum_buf
	xor ch, ch
	mov cl, byte ptr [di+1]
	add di, 2
proc_num:
	call di_smbl_to_dx
	mov bl, 10
	mul bl
	add ax, dx
	inc di
loop proc_num
	mov byte ptr linum, al
	ret

endp



di_smbl_to_dx proc
	;; di contains symbol offset
	mov dl, byte ptr [di]
	mov dh, 10

	cmp dl, '0'
	jl bad_num

	cmp dl, '9'
	jg bad_num
	
	sub dl, '0'

	xor dh, dh
	ret	

bad_num:
	lea dx, error_msg
	call print_s
	pop cx
	pop cx
	mov ah, 4ch
	int 21h	
endp
	
end start

 .286
.model small
.stack 100h

.data
	sop_first_task db "First task: $"
	sop_second_task db 13,10,"Second task: $"
	
	tab db "TAB $"
	escape db "ESC $"
	space db "SPACE $"
	f11 db "F11 $"
	f12 db "F12 $"
	
	zero db "ZERO$"
	one db "ONE$"
	two db "TWO$"
	three db "THREE$"
	four db "FOUR$"
	five db "FIVE$"
	six db "SIX$"
	seven db "SEVEN$"
	eight db "EIGHT$"
	nine db "NINE$"

	newline db 13,10,'$'

.code
Start:	
	mov ax, @data
	mov ds, ax

	lea dx, sop_first_task
	call print_s
input:	
	call getch_no_filter
	
	cmp al, 09h		; tab
	jne $+10
	lea dx, tab
	call print_s
	jmp input
	
	cmp al, 1Bh		; esc
	jne $+10
	lea dx, escape
	call print_s
	jmp input
	
	cmp al, 20h		; space
	jne $+10
	lea dx, space
	call print_s
	jmp input
	
	cmp al, 'q'
	jne $+11
	lea dx, sop_second_task
	call print_s
	jmp second_task
	
	cmp al, 0
	je $+7
	call bell
	jmp input
	
	call getch_no_filter

	cmp al, 85h		; f11
	jne $+10
	lea dx, f11
	call print_s
	jmp input
	
	cmp al, 86h		; f12
	jne $+10
	lea dx, f12
	call print_s
	jmp input

	call bell
jmp input
	
second_task:
	call getch_filter
	cmp al, 'q'
	jne $+5
	jmp fin

	cmp al, '0'		
	jne $+10
	lea dx, zero
	call print_s
	jmp second_task

	cmp al, '1'
	jne $+10
	lea dx, one
	call print_s
	jmp second_task
	
	cmp al, '2'
	jne $+10
	lea dx, two
	call print_s
	jmp second_task
	
	cmp al, '3'
	jne $+10
	lea dx, three
	call print_s
	jmp second_task
	
	cmp al, '4'
	jne $+10
	lea dx, four
	call print_s
	jmp second_task
	
	cmp al, '5'
	jne $+10
	lea dx, five
	call print_s
	jmp second_task
	
	cmp al, '6'
	jne $+10
	lea dx, six
	call print_s
	jmp second_task
	
	cmp al, '7'
	jne $+10
	lea dx, seven
	call print_s
	jmp second_task
	
	cmp al, '8'
	jne $+10
	lea dx, eight
	call print_s
	jmp second_task
	
	cmp al, '9'
	jne $+11
	lea dx, nine
	call print_s
	jmp second_task

	cmp al, 0Dh
	jne $+11
	lea dx, newline
	call print_s
	jmp second_task

	mov dl, al
	call print_c	
jmp second_task
	
fin:	
	mov ah, 4ch
	int 21h


getch_no_filter proc
	mov ah, 07h
	int 21h
	ret
endp

	
getch_filter proc
	mov ah, 08h
	int 21h
	ret
endp

	
print_s PROC
	mov ah, 09h
	int 21h
	ret
endp

	
print_c	proc
	mov ah, 02h 
	int 21h
	ret
endp


bell proc
	pusha
	mov al, 182
	out 43h, al
	mov al, 208
	out 42h, al
	mov al, 17
	out 42h, al
	
	in al, 61h
	or al, 03h
	out 61h, al

	mov cx, 0002h
	mov dx, 0000h 
	mov ah, 86h
	int 15h 
	
	in al, 61h
	and al, 11111100b
	out 61h, al

	popa
	ret
endp
	
end start

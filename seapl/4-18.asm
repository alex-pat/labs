.286
.model small
.stack 100h

.data
	sop_sys_from db "Write source base {2..16}: $"
	sop_sys_to db 13,10,"Write destination base {2..16}: $"
	sop_source db 13,10,"Write number (max 3 digits): $"
	sop_result db 13,10,"Result: $"
	sop_bad_input db 13,10,"Bad input",13,10,'$'
	
	sys_from db 3, 4 dup(?)
	sys_to db 3, 4 dup(?)
	number db 4, 5 dup(?)

	sys_from_bin db ?
	sys_to_bin db ?
	number_bin dw ?
	
.code
Start:	
	mov ax, @data
	mov ds, ax

	lea dx, sop_sys_from
	call print_s
	
	lea dx, sys_from
	call fgets

	lea bx, sys_from_bin
	call to_system
	
	lea dx, sop_sys_to
	call print_s

	lea dx, sys_to
	call fgets

	lea bx, sys_to_bin
	call to_system

	lea dx, sop_source
	call print_s

	lea dx, number
	call fgets
	call num_to_bin


	
	;; call to_number_bin
	lea dx, sop_result
	call print_s
	call print_in_system

	mov ah, 4ch
	int 21h


print_in_system proc
	mov ax, number_bin
	xor cx, cx
	xor bh, bh
	mov bl, sys_to_bin		
pushing:
	xor dx, dx
	div bx

	push dx
	inc cx

	test ax, ax
jnz pushing

	mov ah, 02h
poping:
	pop dx

	cmp bx, 10
	jl $+10			; to 'add dl, 7'
	cmp dl, 10
	jl notABCDEF
	add dl, 7
notABCDEF:	
	add dl, '0'
	int 21h
loop poping
	ret
endp
	

fgets 	proc
	mov ah, 0ah
	int 21h
	ret
endp
	
print_s proc
	mov ah, 09h
	int 21h
	ret
endp

print_c	proc
	mov ah, 02h 
	int 21h
	ret
endp


to_system proc
	;; dx contains string buffer
	;; bx contains bin buffer
	xor ax, ax
	mov di, dx
	cmp byte ptr [di+1], 0
	je bad_sys
	mov ax, word ptr [di+2]
	cmp ah, 13
	je O123
;;; dec..hex
	cmp al, '1'
	jne bad_sys
	cmp ah, '0'
	jl bad_sys
	cmp ah, '6'
	jg bad_sys
	sub ah, '0'
	add ah, 10
	mov byte ptr [bx], ah
	ret
O123:
;;; bin..9th
	cmp al, '2'
	jl bad_sys
	cmp al, '9'
	jg bad_sys
	sub al, '0'
	mov byte ptr [bx], al
	ret

bad_sys:
	lea dx, sop_bad_input
	call print_s
	pop cx
	mov ah, 4ch
	int 21h	
endp	


num_to_bin proc
	;; dx contains buffer
	xor ax, ax		;contains result number
	mov di, dx
	xor ch, ch
	mov cl, byte ptr [di+1]
	add di, 2
proc_num:
	call di_smbl_to_dx
	mul sys_from_bin
	add ax, dx
	inc di
loop proc_num
	mov word ptr number_bin, ax
	ret

endp


di_smbl_to_dx proc
	;; di contains symbol offset
	mov dl, byte ptr [di]
	mov dh, sys_from_bin

	cmp dl, '0'
	jl bad_num

	cmp dh, 10
	jl OI23
;;; dec..hex
	cmp dl, '9'
	jle OI23
	
	cmp dl, 'A'
	jl bad_num
	sub dl, 'A'
	add dl, 10
	jmp cmp_digit_base

OI23:
	sub dl, '0'

cmp_digit_base:	
	cmp dl, dh
	jge bad_num
	xor dh, dh
	ret	

bad_num:
	lea dx, sop_bad_input
	call print_s
	pop cx
	pop cx
	mov ah, 4ch
	int 21h	
endp
	
end start

	.model small
	.stack 100h

	.data
	example1 db 0h
	example2 db 11h, 22h, 63h

	new_line db 0Dh, 0Ah, '$'
	alphabet db 'abcdefghiklmnopqrstuvwxyz$'
	last_name dw 14, 0, 18, 4, 4, 12, 13, 9
	rand_array dw 13, 42, 665, 0, 1488, 11, 146, 228

	final_msg db "odd numbers: $"

	.code
Start:	
	mov ax, @data
	mov ds, ax

;; Adressinng
	mov ax, 3
	push ax
	mov ax, cx
	add example1, al
	mov al, example1
	mov cl, example2[2]
	mov bx, offset alphabet
	mov ax, [bx + 2]
	mov al, byte ptr [bx]
	mov di, 2
	mov ax, [bx + di]

;; lastname output
	lea si, alphabet 
	lea di, last_name 
	mov cx, 8	  	; lname len

output:
	mov bx, [di] 	

 	mov dl, [bx + si]
	call Print_c	

	add di, 2 	
	sub cx, 1
	jnz output

	lea dx, new_line
	call Print_s 	

	lea si, rand_array
	mov cx, 8		; array len
	xor bx, bx		; count of noevens

check_odd:
	and [si], word ptr 1
	add bx, [si]
	add si, 2		;because numbers is words
loop check_odd
	
	lea dx, final_msg
	call Print_s
	mov dl, bl
	add dl, 48		; '0' symbol
	call Print_c

	mov ax, 4c00h
	int 21h

Print_s PROC
	mov ah, 09h
	int 21h
	ret
endp

Print_c	PROC
	mov ah, 02h 
	int 21h
	ret
endp

end start

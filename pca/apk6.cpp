#include <dos.h>
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <io.h>
#include <windows.h>
int ret_code;
int contin = 1;
int bad_ret = 3;

void interrupt (*old_handler)(...);

void indic(int value){
	int time = 300;
	while (time > 0 )
	{
		if (inp(0x64) & 0x02 == 0)
			break;
		time--;
	}
	outp(0x60, 0xED);

	while (bad_ret > 0) {
		if (inp(0x60) == 0xFA)
			break;
		bad_ret--;
	}
	bad_ret = 3;

	time = 300;
	while (time > 0 )
	{
		if (inp(0x64) & 0x02 == 0)
			break;
		time--;
	}
	outp(0x60, value & 7);
}

void interrupt new_handler(...)
{
	ret_code = inp(0x60);
	if (ret_code==0xFE){
		bad_ret--;
		if(bad_ret == 0){
			puts("Error");
			contin = 0;
		}
	}
	if(ret_code == 0xFA){
		bad_ret = 3;
	}
	if(ret_code == 0x01)
	{
	   contin = 0;
	}
	if(ret_code != 0xFA){
		printf("%X\n",ret_code);
	}
	outp(0x20, 0x20);
	outp(0xA0, 0x20);
}

int main(){
	int val_p = val;
	_disable();
	old_handler = getvect(0x9);
	setvect(0x9, new_handler);
	_enable();
	while(contin != 0)
		;
	
	_disable();
	setvect(0x9, old_handler);
	_enable();
	printf("exit");
	return 0;
}
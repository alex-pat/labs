#include <stdio.h>
#include <math.h>
#include <conio.h>
#include <time.h>

int main()
{
    char next = 'y';
    while ( next == 'y' )
    {
        float first, last, step;
        printf("Input first, last value and step: ");
        while ( scanf("%f%f%f", &first, &last, &step) < 3
                                        || first > last )
        {
            fflush(stdin);
            puts("Input error");
        }

        clock_t _time;

        double x, function;
        _time = clock();
	__asm finit;
        for ( x = first; x <= last; x += step)
            __asm
            {
                fld x
                fsqrt
                fld x
                fld x
                fmul
                fld1
                fadd
                fdiv
                fstp function
            }
	__asm fwait;
    _time = clock() - _time;

    printf("asm: %f in %f sec or %d tacts.\n",
                function,
                float(_time) / CLOCKS_PER_SEC,
                _time);

    _time = clock();
    for ( x = first; x <= last; x += step)
        function = sqrt(x)/(x*x+1);
    _time = clock() - _time;

    printf("lib: %f in %f sec or %d tacts.\n",
                function,
                float(_time) / CLOCKS_PER_SEC,
                _time);

        puts("Retry? (y,n)");
        do {
            fflush(stdin);
            next = getch();
        } while ( next != 'y' && next != 'n');
    }
    puts("Press any key");
    getch();
}

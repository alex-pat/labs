#include <stdio.h>
#include <Windows.h>
#include <stdbool.h>
bool initPort(HANDLE *hFile, LPCSTR port);
bool readPort(HANDLE *hFile, char *buffer);

int main()
{
    HANDLE hPortOpenedEvent = OpenEvent(EVENT_ALL_ACCESS, FALSE, "EventOpen"); 
    if(hPortOpenedEvent == NULL)
    {
	fputs("Open port event not created.", stderr);
	return GetLastError();
    }

    HANDLE hPortReadEvent = OpenEvent(EVENT_ALL_ACCESS, FALSE, "EventRead"); 
    if(hPortReadEvent == NULL)
    {
	fputs("Read completed event not created.", stderr);
	return GetLastError();
    }

    HANDLE hPortWriteEvent = OpenEvent(EVENT_ALL_ACCESS, FALSE, "EventWrite"); 
    if(hPortWriteEvent == NULL)
    {
	fputs("Write completed event not created.", stderr);
	return GetLastError();
    }
    
    HANDLE hFile;

    if(initPort(&hFile, "COM2") == false)
    {
	fputs("Could not open port COM2.\n", stderr);
	return GetLastError();
    }

    fputs("COM2 opened successfully.\n", stdout);

    SetEvent(hPortOpenedEvent);

    char buffer[50];

    while(true)
    {
	WaitForSingleObject(hPortReadEvent, INFINITE);

	DWORD numberOfReadBytes;
	if(readPort(&hFile, buffer) == false)
	{
	    fputs("Reading failed", stdout);
	    break;
	}
        if(strcmp(buffer, "exit\n") == 0)
	    break;
	SetEvent(hPortWriteEvent);
    }

    CloseHandle(hFile);
    CloseHandle(hPortOpenedEvent);
    CloseHandle(hPortReadEvent);
    CloseHandle(hPortWriteEvent);

    return 0;
}

bool initPort(HANDLE *hFile, LPCSTR port)
{
    *hFile = CreateFile(port, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);
    if(hFile == NULL)
    {
	return false;
    }

    return true;
}

bool readPort(HANDLE *hFile, char *buffer)
{
    DWORD numberOfWrittenBytes;
    char symbol;
    int i = 0;
    while (ReadFile(*hFile, &symbol, 1, &numberOfWrittenBytes, NULL) == true)
    {
	buffer[i++] = symbol;
	if (symbol == '\0') {
	    printf(buffer);
	    return true;
	}
    }
    return false;
}

#include <stdio.h>
#include <Windows.h>

int main()
{
    HANDLE hFile;
    HANDLE hEvent1;
    HANDLE hEvent2;
    HANDLE hEvent3;
    hEvent1 = CreateEvent(NULL, FALSE, FALSE, "EventOpen");
    hEvent2 = CreateEvent(NULL, FALSE, FALSE, "EventRead");
    hEvent3 = CreateEvent(NULL, FALSE, FALSE, "EventWrite");

    if (hEvent1 == NULL)
    {
	fputs("Event creating error.\n", stderr);
	return 0;
    }
    hFile = CreateFile("COM1", GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);
    if (hFile == NULL)
    {
	fputs("COM1-port opening error.\n", stderr);
	return 0;
    }

    puts("COM1-port opened.");

    WaitForSingleObject(hEvent1, INFINITE);

    puts("COM2-port opened.");

    DWORD numberOfWrittenBytes;

    char buf[50];

    while (1)
    {
	if (fgets(buf, 50, stdin) == NULL)
	    continue;
	WriteFile(hFile, buf, strlen(buf) + 1 , &numberOfWrittenBytes, NULL);
	SetEvent(hEvent2);
	if (strcmp(buf, "exit\n") == 0)
	    break;
	WaitForSingleObject(hEvent3, INFINITE);
    }
    CloseHandle(hFile);
    CloseHandle(hEvent1);
    CloseHandle(hEvent2);
    CloseHandle(hEvent3);
    return 0;
}

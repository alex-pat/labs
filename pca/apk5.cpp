#include <dos.h>
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <io.h>

unsigned int count_ms = 0; 

void _interrupt _far(*old_vector)(...);

void _interrupt _far new_vector(...);

void interrupt(*int_old_alarm) (...);

void interrupt int_new_alarm(...);

int bcd_to_dec (int bcd) { return ((bcd / 16 * 10) + (bcd % 16)); }

int dec_to_bcd (int dec) { return ((dec / 10 * 16) + (dec % 10)); }

void get_time()
{
    outp(0x70, 0x00);
    char second = inp(0x71);
    outp(0x70, 0x02);
    char minutes = inp(0x71);
    outp(0x70, 0x04);
    char hour = inp(0x71);
    outp(0x70, 0x07);
    char day = inp(0x71);
    outp(0x70, 0x08);
    char month = inp(0x71);
    outp(0x70, 0x32);
    char century = inp(0x71);
    outp(0x70, 0x09);
    char year = inp(0x71);
    printf("%02d:%02d:%02d %02d.%02d.%d\n", 
	   bcd_to_dec(hour),
	   bcd_to_dec(minutes),
	   bcd_to_dec(second),
	   bcd_to_dec(day),
	   bcd_to_dec(month),
	   bcd_to_dec(year) + bcd_to_dec(century) * 100);

}

void set_time()
{	
    printf("Write new time:\n");
    int hours;
    do
    {
	while (getchar() != '\n');
	printf("Hours: "); 
	scanf("%d", &hours);
    } while ((hours > 23 || hours < 0));

    int minutes;
    do
    {
	while (getchar() != '\n');
	printf("Minutes: ");  
	scanf("%d", &minutes);
    } while (minutes > 59 || minutes < 0);

    int seconds;
    do
    {
	while (getchar() != '\n');
	printf("Seconds: ");
	scanf("%d", &seconds);
    } while (seconds > 59 || seconds < 0);

    int day;
    do
    {
	while (getchar() != '\n');
	printf("Day: ");
	scanf("%d", &day);
    } while (day > 31 || day < 1);

    int month;
    do
    {
	while (getchar() != '\n');
	printf("Month: ");
	scanf("%d", &month);
    } while (month > 12 || month < 1);

    int year;
    do
    {
	while (getchar() != '\n');
	printf("Year: ");
	scanf("%d", &year);
    } while (year > 9999 || year < 0);
    
    disable();

    int res;
    do
    {
	outp(0x70, 0xA);
	res = inp (0x71) & 0x80;
    } while (res);

    outp(0x70, 0xB);
    outp(0x71, inp(0x71) | 0x80); 
	
    outp(0x70, 0x04);
    outp(0x71, dec_to_bcd(hours));
    outp(0x70, 0x02);
    outp(0x71, dec_to_bcd(minutes));
    outp(0x70, 0x00);
    outp(0x71, dec_to_bcd(seconds));
    outp(0x70, 0x07);
    outp(0x71, dec_to_bcd(day));
    outp(0x70, 0x08);
    outp(0x71, dec_to_bcd(month));
    outp(0x70, 0x09);
    outp(0x71, dec_to_bcd(year % 100));
    outp(0x70, 0x32);
    outp(0x71, dec_to_bcd(year / 100));

    outp(0x70, 0xB);
    outp(0x71, inp(0x71) & 0x7F);

    enable();

    system("cls");
}

void delay(unsigned int ms)
{
    disable();

    old_vector = _dos_getvect(0x70);
    _dos_setvect(0x70, new_vector); 
	
    enable(); 

    outp(0xA1, inp(0xA1) & 0xFE); 

    outp(0x70, 0xB);
    outp(0x71, inp(0x71) | 0x40);  

    count_ms = 0;

    while (count_ms <= ms)
	;

    disable();

    _dos_setvect(0x70, old_vector); 

    enable();

    return;
}

void _interrupt _far new_vector(...)
{
    count_ms++;
    old_vector();
}

void interrupt int_new_alarm(...)
{
    puts("\nWake up!\n");
    int_old_alarm();
    reset_alarm();
}

void set_alarm()
{
    printf("Write alarm time:\n");

    int hour;
    do
    {
	while (getchar() != '\n');
	printf("Hours: ");
	scanf ("%d", &hour);
    } while (hour > 23 || hour < 0);
	
    int minute;
    do
    {
	while (getchar() != '\n');
	printf("Minutes: ");
	scanf ("%d", &minute);
    } while (minute > 59 || minute < 0);
	
    int second;
    do
    {
	while (getchar() != '\n');
	printf("Seconds : ");
	scanf ("%d", &second);
    } while (second > 59 || second < 0);

    disable();
	
    int res;
    do
    {
	outp(0x70, 0xA);
	res = inp (0x71) & 0x80;
    } while (res);

    outp(0x70, 0x05);
    outp(0x71, dec_to_bcd(hour));
    outp(0x70, 0x03);
    outp(0x71, dec_to_bcd(minute));
    outp(0x70, 0x01);
    outp(0x71, dec_to_bcd(second));

    outp(0x70, 0xB);
    outp(0x71, (inp(0x71) | 0x20));

    outp(0xA1, (inp(0xA0) & 0xFE));
	
    int_old_alarm = getvect(0x4A);	
    setvect(0x4A, int_new_alarm);		

    enable();

    printf("OK\n");
}

void reset_alarm()
{
    if (int_old_alarm == NULL)
	return;
	
    disable();

    setvect(0x4A, int_old_alarm);

    int res;
    do
    {
	outp(0x70, 0xA);
	res = inp (0x71) & 0x80;
    } while (res);

    outp(0x70, 0x05);
    outp(0x71, 0x00);

    outp(0x70, 0x03);
    outp(0x71, 0x00);

    outp(0x70, 0x01);
    outp(0x71, 0x00);
    
    outp(0x70, 0xB);
    outp(0x71, (inp(0x71) & 0xDF)); 

    enable();
}

int main()
{
    int ms;
	
    while(1)
    {
	printf("1 - Show time\n");
	printf("2 - Set time\n");
	printf("3 - Set alarm\n");
	printf("4 - Sleep\n");
	printf("q - Quit\n\n");

	switch(getch()) {
	case '1': 
	    get_time(); 
	    break;
			
	case '2': 
	    set_time();
	    break;
		 
	case '3':
	    set_alarm();
	    break;

	case '4': 

	    do
	    {
		while (getchar() != '\n');
		printf("Write ms: ");
		scanf("%d", &ms);
	    } while (ms < 0 || !ms);

	    printf("Sleeping... \n");
	    delay(ms); 
	    printf("Thats all! \n");
	    break;
			 
	case 'q':
	    reset_alarm();
	    return 0;
	}
	
	while (getchar() != '\n');
    }

}


#include <stdio.h>
#include <math.h>
#include <conio.h>
#include <time.h>
#define LENGHT 16

int main()
{
	short vect[LENGHT] = {  0, 1, 2, 3,
							4, 5, 6, 7,
							8, 9,10,11,
						   12,13,14,15 };

	char next = 'y';
	while (next == 'y')
	{
		int count;
		printf("Write count of calculations: ");
		while  (scanf("%d", &count) < 1
						  || count < 0)
		{
			fflush(stdin);
			puts("Input error");
		}

		clock_t _time;
		int x, n, result;

		// pure asm
		_time = clock();
		for (x = 0; x < count; x++)
		{
			n = 16;
			__asm
			{
				pusha
				xor esi, esi
				xor ecx, ecx
			L00P :
				mov ax, vect[esi]
				mov bx, ax

				imul ax, bx
				add cx, ax

				add esi, 2
				sub n, 1
			jnz L00P

				mov result, ecx
				popa
			}
		}
		_time = clock() - _time;

		printf("pure asm : %d in %f sec or %d tacts.\n",
			result,
			(_time) / CLOCKS_PER_SEC,
			_time);

		//asm + MMX
		_time = clock();
		for (x = 0; x < count; x++)
		{
			n = 16;
			__asm
			{
				pusha
				xor esi, esi
				pxor MM7, MM7
			L0OP :
				movq MM0, vect[esi]
				movq MM1, MM0

				pmaddwd MM0, MM1
				paddd MM7, MM0
				add esi, 8
				sub n, 4
			jnz L0OP

				movq MM0, MM7
				psrlq MM7, 32
				paddd MM7, MM0
				movd result, MM7
				popa
			}
		}
		__asm emms;
		_time = clock() - _time;

		printf("asm + MMX: %d in %f sec or %d tacts.\n",
			result,
			(_time) / CLOCKS_PER_SEC,
			_time);

		// C
		int i;
		_time = clock();
		for (x = 0; x < count; x++)
		{
			result = 0;
			for (i = 0; i < 16; i++)
				result += vect[i] * vect[i];
		}
		_time = clock() - _time;

		printf("C : %d in %f sec or %d tacts.\n",
			result,
			(_time) / CLOCKS_PER_SEC,
			_time);

		puts("Retry? (y,n)");
		do {
			fflush(stdin);
			next = _getch();
		} while (next != 'y' && next != 'n');
	}
	puts("Press any key");
	_getch();
}

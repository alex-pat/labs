#include <dos.h>
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

#define SIXTEENTH 5

//notes
#define pd 261.63
#define pdd 277.18
#define pr 293.66
#define prd 311.13
#define pm 329.63
#define pf 349.23
#define pfd 369.99
#define pso 392.0
#define psod 415.30
#define pl 440.0
#define pld 466.16
#define psi 493.88

#define d 523.26
#define dd 554.36
#define r 587.32
#define rd 622.26
#define m 659.26
#define f 698.26
#define fd 739.98
#define so 784.0
#define sod 830.6
#define l 880.0
#define ld 932.32
#define si 987.76

// Массив частот для мелодии
double notes[] =
{
    // swan lake
    m,
    pl, psi, d, r, m,
    d, m, d, m,
    pl, d, pl, pf, d, pl,
    m,
    pl, psi, d, r, m,
    d, m, d, m,
    pl, d, pl, pf, d, pl,
    -1,
    // crawling
    dd,
    si, sod, fd, m, rd, m, dd,
    si, sod, fd, m, rd, m, dd,
    si, sod, fd, m, rd, m, dd,
    si, sod, fd, m, rd, m, dd,

    sod, l, sod, l, sod, 0,
    sod, 0, sod, l, sod, l, sod, fd,
    sod, l, sod, l, sod, 0,
    sod, 0, sod, l, sod, l, sod, fd,
    l, sod, fd, m, rd, m, dd,
    si, sod, fd, m, rd, m,
    
    1000
};

// Массив длительностей
int del[] =
{
    8,
    2, 2, 2, 2, 6,
    2, 6, 2, 6,
    2, 2, 2, 2, 2, 16,
    8,
    2, 2, 2, 2, 6,
    2, 6, 2, 6,
    2, 2, 2, 2, 2, 16,
    1,
    //crawling
    2,
    6, 10, 4, 2, 4, 4, 2,
    6, 10, 4, 2, 4, 4, 2,
    6, 10, 4, 2, 4, 4, 2,
    6, 10, 4, 2, 4, 4, 2,

    6, 2, 6, 2, 14, 1,
    2, 1, 6, 2, 6, 2, 2, 12,
    6, 2, 6, 2, 14, 1,
    2, 1, 6, 2, 6, 2, 2, 12,
    4, 16, 4, 2, 4, 4, 2,
    6, 10, 4, 2, 4, 4
};

void music();
void print(int val);
void printWord();
void random(long val);
void getKD();

int main()
{
	char input = 0;
	
	while (input != 27)
	{
        printf("1 - music\n2 - word\n3 - KD\n4 - random\nESC - exit\n");
        input = getchar();
        while (getchar() != '\n');
		switch (input)
		{
		case '1':
            music();
			break;
		case '2':
			printWord();
			break;
		case '3':
			getKD();
			break;
		case '4':
			long border;
			printf("Border: ");
			scanf("%ld", &border);
			random(border);
            while (getchar() != '\n')
                ;
			break;
		}
	}
	return 0;
}

void music()
{
    for (int i = 0; notes[i] != 1000; i++)
    {
        int cnt;
        static int multipler = 150;
        if (notes[i] == -1) 
        {
            multipler = 140;
            delay(del[i] * multipler);
        }
        else if (notes[i] == 0)
        {
            delay(del[i] * multipler * 0.2);
        }
        else {
            outp(0x43, 0xb6);
            cnt = (int)(1193180 / notes[i]);
            
            outp(0x42, cnt % 256);
            cnt /= 256;
            outp(0x42, cnt);
            
            outp(0x61, inp(0x61) | 3);

            delay(del[i] * multipler);

            outp(0x61, inp(0x61) & 0xfc);
        }
    }
}

void print(int val)
{
	int temp = val % 2;
	static int i = 0;
	if (i == 8) return;
	i++;
	print(val / 2);
	i--;
	printf("%d", temp);
}

void printWord()
{
	for (int iChannel = 0; iChannel < 3; iChannel++)
	{
		if (iChannel == 0)
		{
			printf("word of the first channel: ");
			outp(0x43, 0xE2);
			print(inp(0x40));
			printf("\n");
		}
		else if (iChannel == 1)
		{
			printf("word of the second channel: ");
			outp(0x43, 0xE4);
			print(inp(0x41));
			printf("\n");
		}
		else if (iChannel == 2)
		{
			printf("word of the third channel: ");
			outp(0x43, 0xE8);
			print(inp(0x42));
			printf("\n");
		}
	}

}

void getKD()
{
	long kd, kdHigh, kdLow, kdMax;
	for (int iChannel = 0; iChannel < 3; iChannel++)
	{
		kdMax = 0;
		if (iChannel == 0)
		{
			for (long i = 0; i < 65535; i++)
			{
				outp(0x43, 0x00); 
				kdLow = inp(0x40);
				kdHigh = inp(0x40);
				kd = kdHigh * 256 + kdLow;
				if (kd > kdMax) kdMax = kd;
			}
			printf("KD for first channel: %hx\n", kdMax);
		}
		else if (iChannel == 1)
		{
			for (long i = 0; i < 65535; i++)
			{
				outp(0x43, 0x40);
				kdLow = inp(0x41);
				kdHigh = inp(0x41);
				kd = kdHigh * 256 + kdLow;
				if (kd > kdMax) kdMax = kd;
			}
			printf("KD for second channel: %hx\n", kdMax);
		}
		else if (iChannel == 2)
		{
			for (long i = 0; i < 65535; i++)
			{
				outp(0x43, 0x80);
				kdLow = inp(0x42);
				kdHigh = inp(0x42);
				kd = kdHigh * 256 + kdLow;
				if (kd > kdMax) kdMax = kd;
			}
			printf("KD for third channel: %hx\n", kdMax);
		}
	}
}

void random(long val)
{
	
	long kdLow, kdHigh, kd;
	outp(0x43, 0xB4);
	outp(0x42, val % 256);
	val /= 256;
	outp(0x42,val);
	outp(0x61, inp(0x61) | 1);
	getch();
	outp(0x43, 0x86);
	kdLow = inp(0x42);
	kdHigh = inp(0x42);
	kd = kdHigh * 256 + kdLow;
	printf("Random number : %ld\n", kd);
	outp(0x61, inp(0x61) & 0xFC);
}

#include <dos.h>
#include <stdlib.h>
#include <time.h>

struct VIDEO
{
	unsigned char symb;
	unsigned char attr;
};

void get_reg();		
void print(int, unsigned short, unsigned char);	

void interrupt(*int8) (...);
void interrupt(*int9) (...);
void interrupt(*int74) (...);

// new interrupt handlers
void interrupt int_deadbeef(...)
{
	print (0xDE, 0xB804, (char)rand() );
	print (0xAD, 0xB805, (char)rand() );
	print (0xBE, 0xB806, (char)rand() );
	print (0xEF, 0xB807, (char)rand() );
	get_reg();
	int8();
}

void interrupt int_mouse(...)
{
        get_reg();
	int74();
}

void interrupt int_keyboard(...)
{
        get_reg();
        int9();
}

void print(int val, unsigned short place, unsigned char color)
{
	int i;
	VIDEO far* screen = (VIDEO far *)MK_FP(place, 0);
	for (i = 7; i >= 0; i--)			//find bits
	{						//fill the screen
		screen->symb = val%2 + '0';
		screen->attr = color & 0x7F;
		screen++;
                val >>= 1;
	}
}

//get data from registers
void get_reg()
{
	print(inp(0x21), 0xB800, (char)rand() ); //master mask register

	outp(0x20, 0x0A);			//master request register
	print(inp(0x20), 0xB80A, (char)rand() );

	outp(0x20, 0x0B);			//master service register
	print(inp(0x20), 0xB814, (char)rand() );

	print(0, 0xB81E, 0 );	               
	
	print(inp(0xA1), 0xB828, (char)rand() ); //slave mask register

	outp(0xA0, 0x0A);			//slave request register
	print(inp(0xA0), 0xB832, (char)rand() );

	outp(0xA0, 0x0B);			//master service register
	print(inp(0xA0), 0xB83C, (char)rand() );
}

void init()
{
	//IRQ0-7
	int8 = getvect(0x08);		//timer
	int9 = getvect(0x09);		//keyboard
	int74 = getvect(0x74);

	setvect(0x08, int_deadbeef);            //master
 	setvect(0x09, int_keyboard);
	setvect(0x0A, int_keyboard);
	setvect(0x0B, int_deadbeef);
	setvect(0x0C, int_deadbeef);
	setvect(0x0D, int_deadbeef);
	setvect(0x0E, int_deadbeef);
	setvect(0x0F, int_deadbeef);

	setvect(0x68, int_deadbeef);		// slave
	setvect(0x69, int_deadbeef);
	setvect(0x6A, int_deadbeef);       
	setvect(0x6B, int_deadbeef);
	setvect(0x6C, int_mouse);	
	setvect(0x6D, int_deadbeef);
	setvect(0x6E, int_deadbeef);
	setvect(0x6F, int_deadbeef);

	
	_disable();			//disable interrupts handling (cli)

	//Master
	outp(0x20, 0x11);			//ICW1
	outp(0x21, 0x08);			//ICW2
	outp(0x21, 0x04);			//ICW3
	outp(0x21, 0x01);			//ICW4
        
        //Slave
        outp(0xA0, 0x11);			
	outp(0xA1, 0x68);		
	outp(0xA1, 0x02);			
	outp(0xA1, 0x01);			
  
	_enable();			// enable interrupt handling (sti)	
}

int main()
{
	srand(time(0));
	unsigned far *fp;
	init();

	FP_SEG(fp) = _psp;		//segment
	FP_OFF(fp) = 0x2c;		//offset
	_dos_freemem(*fp);

	_dos_keep(0, (_DS - _CS) + (_SP / 16) + 1); //TSR
	return 0;
}

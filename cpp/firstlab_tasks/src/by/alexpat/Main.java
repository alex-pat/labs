package by.alexpat;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        int count = 1;
        System.out.print("Write count of elements: ");

        do {
            count = intInput();
        } while ( count < 3);

        int[] intArray = new int[count];

        for ( int i = 0; i < count; i++)
            intArray[i] = intInput();


        boolean up;
        if (intArray[0] <= intArray[1])
            up = true;
        else
            up = false;

        int i;
        for (i = 2; i < count; i++)
            if (intArray[i - 1] <= intArray[i] ^ !up)
                break;

        if (i == count) {
            System.out.println("Originally array sorted");
            System.exit(0);
        }

        Arrays.sort(intArray);

        for ( i = 0; i < count; i++)
            System.out.print( intArray[i] + " " );

    }

    public static int intInput () {
        Scanner scan = new Scanner(System.in);
        int number = 0;
        while ( !scan.hasNextInt() )
            scan.next();
        number = scan.nextInt();
        return number;
    }

}

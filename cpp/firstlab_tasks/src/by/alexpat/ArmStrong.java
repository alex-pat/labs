package by.alexpat;

import java.util.Scanner;

/**
 * Created by postskript on 2/15/16.
 */
public class ArmStrong {

    public static void main (String[] args) {
        while ( true ) {
            int number = intInput();
            if ( number < 0 )
                System.exit(0);
            else
                System.out.println( isArmStrong(number)? "Yes" : "No");
        }
    }

    public static boolean isArmStrong(int number) {
        int result = 0;
        int old = number;
        int orig = number;
        int count = 0;
        do {
            count++;
        } while( (number /= 10) != 0);
        while ( orig != 0) {
            int remainder = orig % 10;
            result += Math.pow(remainder, count);
            orig /= 10;
        }
        if (old == result){
            return true;
        }
        return false;
    }

    public static int intInput () {
        Scanner scan = new Scanner(System.in);
        int number = 0;
        while ( !scan.hasNextInt() )
            scan.next();
        number = scan.nextInt();
        return number;
    }
}

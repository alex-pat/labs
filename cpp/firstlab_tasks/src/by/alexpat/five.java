package by.alexpat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

/**
 * Created by postskript on 2/15/16.
 */
public class five {
    public static void main(String[] args) {

        int count = 1;
        System.out.print("Write count of elements: ");

        do {
            count = intInput();
        } while ( count < 3);

        ArrayList<Integer> intArray = new ArrayList<Integer>();

        for ( int i = 0; i < count; i++)
            intArray.add(i, new Integer(intInput()));

        boolean up;
        if (intArray.get(1) <= intArray.get(2))
            up = true;
        else
            up = false;

        int i;
        for (i = 2; i < count; i++)
            if (intArray.get(i-1) <= intArray.get(i) ^ !up)
                break;

        if (i == count) {
            System.out.println("Originally array sorted");
            System.exit(0);
        }

        Collections.sort(intArray);

        System.out.print( intArray.toString() );

    }
    public static Integer intInput () {
        Scanner scan = new Scanner(System.in);
        int number = 0;
        while ( !scan.hasNextInt() )
            scan.next();
        number = scan.nextInt();
        return new Integer(number);
    }
}

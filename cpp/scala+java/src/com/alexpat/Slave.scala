package com.alexpat

class Slave ( val a: Int, val b: Int ) {

  def min(): Int = {
    if (a < b ) a else b
  }

  def fiboRec( n : Int) : Int = n match {
    case 0 | 1 => n
    case _ => fiboRec(n - 1) + fiboRec(n - 2)
  }

  def fiboNoRec( n : Int ) : Int = {
    var a = 0
    var b = 1
    var i = 0

    while( i < n ) {
      val c = a + b
      a = b
      b = c
      i = i + 1
    }
    a
  }
}
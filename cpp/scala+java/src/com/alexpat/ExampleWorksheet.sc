val numbers = List( -4, -3, -2, -1, 0, 1, 2, 3, 4)

( numbers.map((i: Int) => i * 2) ).foreach((i: Int) => print(i + " "))

( numbers.filter((i: Int) => i < 0 ) ).foreach((i: Int) => print(i + " "))

(List(1, 2, 3).zip(List("a", "b", "c"))).foreach( x => println (x._1 + " " + x._2))

numbers.foldLeft(0) { (m: Int, n: Int) => println("m: " + m + " n: " + n); m * n }

numbers.foldRight(0) { (m: Int, n: Int) => println("m: " + m + " n: " + n); m * n }

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "memman.h"

mem_ptr pointers[MAXMEMSIZE];

int get_yet_ptr_index()
{
    int i;
    for (i = 0; i < MAXMEMSIZE; i++)
	if (pointers[i].is_busy == 0)
	    return i;
    return -1;
}

int main()
{
    __init_mem__();
    
    int i;
    char c = 0;

    while(c != 'q')
    {		
	if (c == 'm')
	{
	    int size;
	    printf("size = ");
	    while (scanf("%d", &size) < 1
		   && size < 1)
		while (getchar() != '\n');
	    i = get_yet_ptr_index();
	    
	    if ((pointers[i].block = __malloc__(size)) == NULL )  
	    {
		puts("cannot allocate");
		continue;
	    }
	    pointers[i].is_busy = 1;

	    memset(pointers[i].block->mem_pointer,
		   'a' + i,
		   size);
	}

	if (c == 'f')
	{
	    printf("at what index = ");
	    int index = 0;
	    while (scanf("%d", &index) < 1
		   && index < 0)
		while (getchar() != '\n');

	    for (i = 0; i < MAXMEMSIZE; i++)
		if(pointers[i].is_busy &&
		   pointers[i].block->index == index )
		{
		    memset(pointers[i].block->mem_pointer,
			   0,
			   pointers[i].block->len);

		    __free__(pointers[i].block->mem_pointer);
		    pointers[i].block = NULL;
		    pointers[i].is_busy = 0;
		    break;
		}				
	}

	if (c == 'd')
	    __defrag__();

	if (c == 'r')
	{
	    printf("At what index = ");
	    int index = 0;
	    while (scanf("%d", &index) < 1
		   && index < 0)
		while (getchar() != '\n');

	    printf("New size = ");
	    int size = 0;
	    while (scanf("%d", &size) < 1
		   && index < 1)
		while (getchar() != '\n');

	    for (int i = 0; i < MAXMEMSIZE; i++)
		if (pointers[i].is_busy &&
		    pointers[i].block->index == i)
		{
		    pointers[i].block =
			__realloc__(pointers[i].block->mem_pointer,
				    size);
		    break;
		}
	}
	system("clear");

	__view_mem__();
	__stat__();

	puts("m=malloc f=free r=realloc d=defrag q=quit");
	c = getchar();
	while (getchar() != '\n');
    }
    __freeall__();
    return 0;
}

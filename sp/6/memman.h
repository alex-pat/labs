#include <stdint.h>

#define MAXMEMSIZE 100

typedef struct __memblock__
{
    uint8_t is_free;
    uint8_t index;
    uint8_t len;
    uint8_t* mem_pointer;
    struct __memblock__ *next;
    struct __memblock__ *prev;
} mblock_t; 

typedef struct __pointer__
{
    uint8_t is_busy;
    mblock_t* block;
} mem_ptr;

extern mem_ptr pointers[MAXMEMSIZE];

void __init_mem__();
mblock_t* __malloc__(uint8_t size);
mblock_t* __realloc__(void* ptr, uint8_t size);
void __free__ (void* ptr);
void __freeall__();
void __stat__();
void __view_mem__();
void __defrag__();

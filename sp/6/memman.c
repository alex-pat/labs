#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "memman.h"

uint8_t mem[MAXMEMSIZE];
mem_ptr pointers[MAXMEMSIZE];

mblock_t *list = NULL;

mblock_t* get_block(void* pointer)
{
    mblock_t* iter = list;
    while (iter)
	if (iter->mem_pointer == pointer)
	    return iter;
        else
	    iter = iter->next;
    return NULL;
}

void __defrag__()
{
    mblock_t * iter = list;
    if (iter == NULL)
	return;
    
    while (iter->next)
    {
	if (iter->is_free && iter->next->is_free)
	{
	    mblock_t* second = iter->next;

	    iter->len += second->len;
	    iter->next = second->next;
	    if (second->next != NULL)
		second->next->prev = iter;

     	    free(second);	    
	    continue;
	}
	
	if (iter->is_free && !(iter->next->is_free) )
	{
	    uint8_t i;
	    for (i = 0;
		 i < iter->next->len;
		 i++)
	    {
		mem[ iter->index + i ] = iter->next->mem_pointer[i];
	    }
	    i += iter->index;
	    for ( ; i < (iter->next->index + iter->next->len); i++)
		mem[i] = 0;
	    iter->is_free = 0;
	    iter->next->is_free = 1;

	    for (i = 0; i < MAXMEMSIZE; i++)
		if (pointers[i].block == iter->next)
		{
		    pointers[i].block = iter;
		    break;
		}

	    i = iter->len;
	    iter->len = iter->next->len;
	    iter->next->len = i;

	    iter->next->index = iter->index + iter->len;
	    	    
	    iter->mem_pointer = mem + iter->index;
	}
	iter = iter->next;
    }
}

void __init_mem__()
{
    list = (mblock_t *)malloc(sizeof(mblock_t));
    list->is_free = 1;
    list->index = 0;
    list->len = MAXMEMSIZE;
    list->next = NULL;
    list->prev = NULL;
    list->mem_pointer = NULL;
}

mblock_t* __malloc__(uint8_t size)
{
    mblock_t * iter = list;

    while(iter)
    {
	if(iter->is_free)
	{
	    if(iter->len > size)
	    {
		mblock_t * new_block = (mblock_t *)malloc(sizeof(mblock_t));
		new_block->is_free = 0;
		new_block->len = size;
		new_block->index = iter->index;
		new_block->next = iter;
		new_block->prev = iter->prev;
		if (new_block->prev == NULL)
		    list = new_block;
		else
		    new_block->prev->next = new_block;
		new_block->mem_pointer = mem + iter->index;
		iter->prev = new_block;
		iter->index += size;  
		iter->len -= size;
	     
		    
		return new_block;
	    }

	    if(iter->len == size)
	    {
		iter->is_free = 0;
		iter->mem_pointer = mem + iter->index;

		return iter;
	    }
	}

	iter = iter->next;
    }
    return NULL;
}

void __free__(void * fr_pointer)
{
    mblock_t * block_for_del = get_block(fr_pointer);
    if (block_for_del != NULL)
    {
	block_for_del->is_free = 1;
	block_for_del->mem_pointer = NULL;
	for (uint8_t i = block_for_del->index;
	     i < block_for_del->len; i++)
	{
	    mem[i] = 0;
	}
    } 
}

mblock_t* __realloc__(void* ptr, uint8_t size)
{
    mblock_t* new_ptr_blk = __malloc__(size);
    if (new_ptr_blk == NULL)
	return get_block(ptr);
    memcpy(new_ptr_blk->mem_pointer,
	   ptr,
	   get_block(ptr)->len);
    __free__(ptr);
    return new_ptr_blk;
}

void __freeall__()
{
    mblock_t* rem;
    while (list)
    {
	rem = list;
	list = list->next;
	free(rem);
    }
}

void __stat__()
{
    mblock_t* p = list;	

    printf("%5s %4s %3s %3s \n", "type", "beg", "end", "size");
	
    while(p)
    {       
	printf("%s  %4d %3d %3d\n",
	       p->is_free ? "free" : "busy",
	       p->index,
	       p->index + p->len -1,
	       p->len);
	p = p->next;
    }
}

void __view_mem__()
{
    for (uint8_t i = 0; i < MAXMEMSIZE; i++) 
	printf("%c", mem[i] ? mem[i] : '.');

    putchar('\n');
}

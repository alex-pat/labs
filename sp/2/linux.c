#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <termios.h>
#include <stdint.h>
#include "lab2lib.h"

int semaphore_id;
key_t ftok_key;
struct sembuf first_buf;
struct sembuf second_buf;

void print(uint8_t number_of_proc);

void init()
{
    ftok_key = ftok("./sp2", 0);
    semaphore_id = semget(ftok_key, 1, 0666 | IPC_CREAT);
    semctl(semaphore_id, 0, SETVAL, 0);
	first_buf.sem_num = 0;
	second_buf.sem_num = 0;
	second_buf.sem_op = 1;
}

uint16_t get_input()
{
	char ch;
	struct termios old, new;
	tcgetattr(0, &old);
	new = old;
	new.c_lflag &= ~(ICANON | ECHO);
	tcsetattr(0, TCSANOW, &new);
	ch = getchar();
	tcsetattr(0, TCSANOW, &old);
	return ch;
}

void add_process(stack** processes)
{
    uint8_t number = (*processes == NULL)
                        ? 0
                        : (*processes)->stack_size;

    pid_t pid = fork();
	switch (pid)
    {
		case 0:
		    print(number);
			break;

		case -1:
			perror("sp2");
			exit(EXIT_FAILURE);

		default:
			push(processes, pid);
			break;
	}
	return;
}

void kill_process(stack** processes)
{
    if ( (*processes) == NULL )
        return;
    pid_t pid = pop(processes);
	semop(semaphore_id, &first_buf, 1);
	kill(pid, SIGKILL);
	waitpid(pid, NULL, 0);
	return;
}

void end(stack** processes)
{
    pid_t pid;
    semop(semaphore_id, &first_buf, 1);
    semop(semaphore_id, &second_buf, 1);
    while (*processes != NULL)
    {
        pid = pop(processes);
        kill(pid, SIGKILL);
        waitpid(pid, NULL, 0);
    }
	semctl(semaphore_id, 0, IPC_RMID, 0);
}

void print(uint8_t number_of_proc)
{
    pid_t pid = getpid();
	for ( ; ; )
    {
		semop(semaphore_id, &first_buf, 1);
		semop(semaphore_id, &second_buf, 1);
		usleep(10000);
		printf("%d: pid = %s\n", number_of_proc, pid);
		second_buf.sem_op = -1;
		semop(semaphore_id, &second_buf, 1);
		second_buf.sem_op = 1;

		usleep(10000);
	}
    exit(EXIT_SUCCESS);
	return;
}

#include "lab2lib.h"
#include <stdlib.h>

int main(int argc, char const *argv[]) {
    stack* processes = NULL;
    init();
    while (1)
    {
        uint16_t input;
        input = get_input();
        switch (input) {
            case '+':
                add_process(&processes);
                break;
            case '-':
                kill_process(&processes);
                break;
            case 'q':
                end(&processes);
                return EXIT_SUCCESS;
        }
    }
    end(&processes);
    return EXIT_SUCCESS;
}

#include "stack.h"
#include <stdio.h>
#include <stdlib.h>

void push (stack** stk, procinfo value)
{
    stack* new_item = (stack*) calloc (sizeof( stack), 1);
    if (new_item == NULL)
    {
       printf("Error");
       exit(1);
    }
    new_item->process_info = value;
    new_item->stack_size = (*stk)? (*stk)->stack_size + 1
                                 : 0;
    new_item->next = *stk;
    *stk = new_item;
}

procinfo pop (stack** stk)
{
    if (stk == NULL)
        return -1;
    stack* rem = *stk;
    procinfo value = rem->process_info;
    *stk = (*stk)->next;
    free(rem);
    return value;
}

void free_stack(stack** stk)
{
    while ( *stk != NULL )
        pop(stk);
}

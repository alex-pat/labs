#include <inttypes.h>

#ifdef __linux__
#include <unistd.h>
typedef pid_t procinfo;

#else
#include <Windows.h>
typedef struct procinfo
{
	STARTUPINFO startupInfo;
	PROCESS_INFORMATION procInfo;
};
#endif

typedef struct stack
{
    procinfo process_info;
    uint8_t stack_size;
    struct stack *next;
} stack;

procinfo pop (stack** stk);
void push (stack** stk, procinfo value);
void free_stack(stack** stk);

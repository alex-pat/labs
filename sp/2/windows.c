#include <iostream>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX_COUNT 10
#define RADIX 10

void print(int);

using std::cout;
using std::endl;

static const char * processes[] = { "1. First process\r\n", "2. Second process\r\n",
                                    "3. Third process\r\n", "4. Fourth process\r\n",
                                    "5. Fifth process\r\n", "6. Sixth process\r\n",
                                    "7. Seventh process\r\n", "8. Eighth process\r\n",
                                    "9. Ninth process\r\n", "10. Tenth process\r\n" };

#include <Windows.h>
#include <conio.h>

#define _CRT_SECURE_NO_WARNINGS
#define SHORT_SLEEP 1
#define LONG_SLEEP 50
#define HANDLE_MACRO Process

int getch_noblock();

void print(int numberOfProcess)
{
	for (int i = 0; i < strlen(processes[numberOfProcess - 1]); i++)
	{
		cout << processes[numberOfProcess - 1][i];
		Sleep(LONG_SLEEP);
	}

	return;
}

void init()
{
    char key;
	int numOfProcess = 0;
	bool flag = false;
	int currentNum = 1;
	// array of processes (okay, sounds not very correctly, but it is)
	HANDLE_MACRO process[MAX_COUNT];


    return;
}

uint16_t get_input()
{
    return getch();
}

void add_process()
{
    if (numOfProcess < MAX_COUNT)
	{
		numOfProcess++;
		process[numOfProcess - 1].createNewProcess(value[0], numOfProcess);
	}
    return;
}

void kill_process()
{
    if (numOfProcess > 0)
    {
    	SetEvent(process[--numOfProcess].getCloseEvent());
    }
    return;
}

void end()
{
    if (numOfProcess > 0)
	{
		for (int i = 0; i < numOfProcess; i++)
		{
		    SetEvent(process[i].getCloseEvent());
		}
    }
}

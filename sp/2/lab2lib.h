#include <stdint.h>
#include "stack.h"

void init();

uint16_t get_input();

void add_process(stack** processes);

void kill_process(stack** processes);

void end(stack** processes);

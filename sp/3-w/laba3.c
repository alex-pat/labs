#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define BUFSIZE 1024

int main(int argc, char* argv[])
{
	HANDLE hFileMap;
	HANDLE hEvent1;
	HANDLE hEvent2;

	void* pvShareBuf;

	STARTUPINFO si = { sizeof(si) };
	PROCESS_INFORMATION pi;

	hEvent1 = CreateEvent(NULL, FALSE, FALSE, "laba3_hEvent1");
	if (hEvent1 == NULL) return EXIT_FAILURE;

	hEvent2 = CreateEvent(NULL, FALSE, FALSE, "laba3_hEvent2");
	if (hEvent2 == NULL) return EXIT_FAILURE;

	hFileMap = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, BUFSIZE, "laba3_hFileMap");

	if (hFileMap == NULL) return EXIT_FAILURE;

	pvShareBuf = MapViewOfFile(hFileMap, FILE_MAP_ALL_ACCESS, 0, 0, BUFSIZE);
	if (pvShareBuf == NULL) return EXIT_FAILURE;

	if (argc == 1)
	{	//parent == server
		CreateProcess(NULL, "laba3.exe client" , NULL, NULL, FALSE, CREATE_NEW_CONSOLE, NULL, NULL, &si, &pi);		

		while(1)
		{
		
			printf("> ");
			scanf("%s",(char*) pvShareBuf);
		
			SetEvent(hEvent1);
					
			WaitForSingleObject(hEvent2, INFINITE);

			if (((CHAR*)pvShareBuf)[0] == 'q') break;
		}
	} 
	else
	{	//child == client
		while(1)
		{		
			WaitForSingleObject(hEvent1, INFINITE);

			printf("< %s\n", pvShareBuf);
			
			SetEvent(hEvent2);

			if (((CHAR*)pvShareBuf)[0] == 'q') break;
		}
	}


	CloseHandle(hEvent1);
	CloseHandle(hEvent2);
	CloseHandle(hFileMap);
	UnmapViewOfFile(pvShareBuf);
	CloseHandle(pi.hThread);
	CloseHandle(pi.hProcess);
return EXIT_SUCCESS;
}


/*
	HANDLE hTmpFile;
	hTmpFile = CreateFile(	"tmp.dat",
							GENERIC_READ | GENERIC_WRITE,
							FILE_SHARE_READ | FILE_SHARE_WRITE, 
							NULL,
							OPEN_ALWAYS | CREATE_NEW, 
							FILE_ATTRIBUTE_NORMAL, 
							NULL);
	if (hTmpFile == INVALID_HANDLE_VALUE) return EXIT_FAILURE;
	CloseHandle(hTmpFile);
*/

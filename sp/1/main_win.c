#include <windows.h>
#include <stdio.h>
#include <time.h>

void DrawTime(TCHAR* message);

int main(int argc, TCHAR *argv[])
{

    if (argc == 1)
    {

    	PROCESS_INFORMATION proc_info;
        STARTUPINFO start_info;

        ZeroMemory(&start_info, sizeof(STARTUPINFO));
        start_info.cb = sizeof(STARTUPINFO);

        TCHAR cmd_line[] = TEXT("New process call");
    	BOOL mySuccess = CreateProcess (argv[0],
					cmd_line,
					NULL,
					NULL,
					FALSE,
					0,
					NULL,
					NULL,
					&start_info,
					&proc_info);
    	if (mySuccess)
    	{
    		CloseHandle(proc_info.hThread);
    		CloseHandle(proc_info.hProcess);

    		WaitForSingleObject(proc_info.hProcess, INFINITE);
    	}
    	else
    	{
    		printf("Error in creating process\n");
    		return EXIT_FAILURE;
    	}
        Sleep(500);
        while ( TRUE )
        {
    	   DrawTime(TEXT("Parent"));
           system("cls");
       }
    }
    else
        while ( TRUE )
    	   DrawTime(TEXT("Child"));

    return EXIT_SUCCESS;
}

void DrawTime(TCHAR* lpString)
{
	time_t t;
	struct tm* lt;

	time(&t);
	lt = localtime(&t);
	printf("%-6s \n%s", lpString, asctime(lt));
	Sleep(1000);
}

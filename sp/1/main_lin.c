
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <ncurses.h>
#include <time.h>
#include <stdint.h>
#include <stdlib.h>

void print_time( int8_t y, int8_t x, const char* process_info );

int main(int argc, char const *argv[]) {

    initscr();
    noecho();
    keypad(stdscr, true);
    halfdelay(10);
    mvwaddstr(stdscr, 1, 15, "Press F1 to exit");
    move(0, 0);

    pid_t pid = fork();
    if ( pid == -1 )
    {
        printf("fork error!\nexit");
        return EXIT_FAILURE;
    }
    while ( true )
    {
        if ( pid == 0)
            print_time( 6, 11, "Child process");
        else
	{
            print_time( 3, 11, "Parent process");
	    waitpid( pid, NULL, WNOHANG);
	}
        int ch = getch();
        if (ch == KEY_F(1))
            break;
    }
    if ( pid != 0 )
    {
	wait(0);
        endwin();
    }
    return EXIT_SUCCESS;
}

void print_time( int8_t y, int8_t x, const char* process_info)
{
    WINDOW* window = newwin( 2, 40, y, x );

    time_t _time;
    time(&_time);

    mvwaddstr(window, 0, 5, process_info);
    mvwaddstr(window, 1, 0, ctime(&_time) );

    wrefresh(window);
    delwin(window);
    refresh();
}

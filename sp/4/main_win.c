#include <stdio.h>
#include <windows.h>
#include <conio.h>

#define MAX 100

DWORD WINAPI thread_print(LPVOID lp_param);
CRITICAL_SECTION critical_section;

int main()
{
    DWORD thread_id;
    char input;
    HANDLE handle_array[MAX];
    int thread_number = 0;

    InitializeCriticalSection(&critical_section);

    while (1)
    {
	input = _getch();
	switch (input)
	{
	case '+':
	    thread_number++;
	    if (thread_number <= MAX)
	    {
		handle_array[thread_number] = (HANDLE)
		    CreateThread(NULL,
				 0,
				 thread_print,
				 (void*)thread_number,
				 0,
				 &thread_id);
	    } else {
		thread_number--;
		puts("No more threads");
	    }
	    break;
	case '-':
	    if (thread_number >= 0)
	    {
		EnterCriticalSection(&critical_section);
		TerminateThread(handle_array[thread_number], NO_ERROR);
		thread_number--;
		LeaveCriticalSection(&critical_section);
	    } else {
		puts("There aren't any threads");
	    }
	    break;
	case 'q':
	    EnterCriticalSection(&critical_section);
	    while (thread_number >= 0)
	    {
		TerminateThread(handle_array[thread_number], NO_ERROR);
		thread_number--;
	    }
	    LeaveCriticalSection(&critical_section);
	    DeleteCriticalSection(&critical_section);
	    return 0;
	}
    }
}

DWORD WINAPI thread_print(LPVOID lp_param)
{
    while (1)
    {
	char string[30];
	sprintf(string, "Thread %d is running", (int)lp_param);

	EnterCriticalSection(&critical_section);
	for (int i = 0; i < strlen(string); i++)
	{
	    putchar(string[i]);
	    Sleep(20);
	}
	putchar('\n');
	LeaveCriticalSection(&critical_section);
	Sleep(500);
    }
    return 0;
}

#define _XOPEN_SOURCE 600
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <string.h>
#include <unistd.h>
#include <termios.h>

const int MAX = 10;

int thread_number = -1;

pthread_mutex_t mutex;

char _getch();

void* thread_print(void*);

int main()
{
    char ch;
	
    pthread_t thread[MAX];

    pthread_mutex_init(&mutex, NULL);

    while(1)
    {		
	ch = _getch();
	switch(ch)
	{
	case '+':
	    thread_number++;
	    if(thread_number < MAX-1)
	    {
		if (pthread_create(&thread[thread_number],
				   NULL,
				   thread_print,
				   &thread_number) != 0)
		    puts("Error of creating thread");
	    }
	    break;
	case '-':
	    if(thread_number >= 0)
	    {
		pthread_mutex_lock(&mutex);
		pthread_cancel(thread[thread_number]);
		pthread_mutex_unlock(&mutex);
		thread_number--;
	    }
	    else
		printf("There aren't any threads");
	    break;
	case 'q':
	    while(thread_number > 0)
	    {
		pthread_mutex_lock(&mutex);
		pthread_cancel(thread[thread_number]);
		pthread_mutex_unlock(&mutex);
		thread_number--;
	    }
	    pthread_mutex_destroy(&mutex);
	    return 0;
	    break;
	}

    }
    return 0;
}

void* thread_print(void *arg)
{ 
    int i = 0;
    char string[30];
    int id = * (int *) arg;

    sprintf(string, "Thread %d is running", id+1);
    while(1)
    {

	pthread_mutex_lock(&mutex);
	for(i = 0; i < strlen(string); i++)
	{
	    printf("%c",string[i]);
	    usleep(30000);
	    fflush(stdout);
	}
	printf("\n");		
	pthread_mutex_unlock(&mutex);
	sleep(1);
	
    }
    pthread_exit(NULL);
}

char _getch() 
{ 
    struct termios old, new; 
    char ch; 
    tcgetattr(0, &old); 
    new = old; 
    new.c_lflag &= ~(ICANON | ECHO); 
    tcsetattr(0, TCSANOW, &new); 
    ch = getchar(); 

    tcsetattr(0, TCSANOW, &old); 
    return ch; 
}


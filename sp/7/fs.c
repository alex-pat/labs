#include "device.h"
#include "fs.h"

unsigned char map[SECTOR_SIZE];
unsigned char nodes[SECTOR_SIZE];

char** split(char* path) 
{
    puts("split started");

    char* buf = (char*)malloc(strlen(path) + 1);
    memcpy(buf, path, strlen(path) + 1);

    char** res;
    if (strlen(buf) > 1) {
	int count = 0;
	int i = 0;
	while(buf[i] != 0)
	    if (buf[i++] == '/') 
		count++;
	res = malloc(sizeof(char*)*(count + 2));
	res[count + 1] = 0;
	res[0] = "/";
	char* pointer = strtok(buf, "/");
	i = 1;
	while(pointer) {
	    res[i++] = pointer;
	    pointer = strtok(NULL, "/");
	}
    } else {
	res = (char**)malloc(sizeof(char*)*2);
	res[1] = NULL;
	res[0] = "/";
    } 

    puts("split ended");
    return res;
}

node_t* find_node_by_name (const char* pth)
{
    printf("%s: %s\n", __func__, pth);

    if (strcmp(pth, "/") == 0)
    {
	node_t* res_node = (node_t*)malloc(sizeof(node_t));
	res_node->name[0] = '/';
	res_node->name[1] = '\0';
	res_node->size = -2;
	res_node->sectors[0] = 3;
	printf("node: %s, size: %d\n", res_node->name, res_node->size);
	return res_node;
    }

    char *path = (char*) malloc (strlen(pth)+1);
    strncpy(path, pth, strlen(pth)+1);
    char **names = split(path);

    int i;
    for (i = 0; names[i] != NULL; i++) {
        puts(names[i]); 
    }
    int path_index = 1;
    char f_name [7];
    unsigned char f_sector [SECTOR_SIZE];
    int  f_sector_index = 3;
    do
    {
	device_read_sector(f_sector, f_sector_index);

	printf("readed %d\n", f_sector_index);
	for (i = 0; i < SECTOR_SIZE &&
		 f_sector[i] != 0; i += 8)
	{
	    unsigned char* buf = f_sector + i;
	    int im = -1; do{im++;printf("%c(%d) ",buf[im],buf[im]);}while(buf[im]);putchar('\n');
	    memcpy(f_name, f_sector + i, 7);
	    printf("cmp in FNBN: %s %s\n", f_name, names[path_index]);
	    if (strncmp(f_name, names[path_index], 6) == 0)
		break;
	}
	if (i == MAX_FILES_IN_DIR || f_sector[i] == 0) 
	{
	    puts("node not found");
	    node_t* err = (node_t*)malloc(sizeof(node_t));
	    err->size = -1;
	    return err;
	}
	if ( *((int16_t*)(nodes + NODE_LEN * f_sector[i+7] + 6)) != -2
	     && names[path_index+1] != NULL)
	{
	    puts("file on the spot of the dir");
	    node_t* err = (node_t*)malloc(sizeof(node_t));
	    err->size = -1;
	    return err;
	}
	f_sector_index = nodes[NODE_LEN * f_sector[i+7] + 8];
	path_index++;
    } while (names[path_index]);
    puts("search ended");
    int node_index = f_sector[i+7];
    printf("node_index = %d\n", node_index);
    node_t* res_node = (node_t*)malloc(sizeof(node_t));
    memcpy( &(res_node->name[0]), &(nodes[node_index*32]),6);
    memcpy( &(res_node->size), &(nodes[node_index*32+6]),2);
    memcpy( &(res_node->sectors[0]), &(nodes[node_index*32+8]),24);
    printf("node: %s, size: %d\n", res_node->name, res_node->size);
    return res_node;
}

node_t* find_parent_node(const char* pth)
{
    printf("%s: %s\n", __func__, pth);

    char *path = (char*)malloc(strlen(pth)+1);
    strncpy(path, pth, strlen(pth)+1);

    int i;
    int slash;
    for (i = 0; i < strlen(path); ++i)
	if (path[i] == '/')
	    slash = i;
    if (slash == 0)
	return find_node_by_name("/");
    path[slash] = '\0';
    return find_node_by_name(path);
}

void fs_read_file(unsigned char* buffer, unsigned char* sectors)
{
    printf("%s\n", __func__);

    for (int i = 0; sectors[i] != 0; ++i)
    {
	device_read_sector(buffer, sectors[i]);
	buffer += SECTOR_SIZE;
    }
}

file_info_t* get_file_info_by_node(node_t* f_node)
{
    printf("%s\n", __func__);
    file_info_t* result = (file_info_t*)malloc(sizeof(file_info_t));
    result->f_node = f_node;
    result->need_update = 0;
    fs_read_file( result->f_data, f_node->sectors );
    return result;
}

void fs_update_map()
{
    printf("%s\n", __func__);
    printf("Writing map to disc ...\n");
    device_write_sector(map, 1);
    device_flush();
}

void fs_update_nodes()
{
    printf("%s\n", __func__);
    printf("Writing nodes to disc ...\n");
    device_write_sector(nodes, 2);
    device_flush();
}

int get_new_node_offset()
{
    int i;
    for (i = 0; i < SECTOR_SIZE; i += 32)
	if( nodes[i] == 0)
	    return i;
    return -1;
}

int write_node_info_to_parent_sector(char* name, int s_number, int n_number)
{	
    printf("%s: %s, %d %d\n", __func__, name, s_number, n_number);
    unsigned char parent_sector[SECTOR_SIZE];
    device_read_sector(parent_sector, s_number);
    int i;
    for (i = 0; i < SECTOR_SIZE; i += 8)
	if (parent_sector[i] == 0)
	{	
	    memcpy(parent_sector + i, name, MAX_NAME_LEN);
	    parent_sector[i + 7] = (uint8_t) n_number;
	    device_write_sector(parent_sector, s_number);
	    device_flush();
	    return 0;
	}
    return -1;
}

int fs_get_free_sector()
{
    int i = 4;
    while ( (map[i] != 0) && (i < SECTOR_SIZE) )
	i++;
    return (map[i] != 0) ? -1 : i;
}

int fs_sectors_count(int size)
{
    int count = size / SECTOR_SIZE;
    if (size % SECTOR_SIZE != 0)
	count++;
    return count;
}

int fs_grow_file(file_info_t* fi, int new_size)
{
    node_t* node = fi->f_node;
    int i;
    for (i = 0; node->sectors[i] != 0; i++)
	;

    int new_sectors_count = fs_sectors_count(new_size);
    int old_sectors_count = fs_sectors_count(node->size);
    if (new_sectors_count > MAX_SECTORS_PER_FILE)
	return -ENOSPC;

    while ( old_sectors_count < new_sectors_count) 
    {
	int sector = fs_get_free_sector();
	if (sector == -1) 
	    return -ENOSPC;
	map[sector] = 0xFF;
	node->sectors[old_sectors_count] = sector;
	old_sectors_count++;
	printf("Allocating block %d\n", sector);
    }
    int offset = 0;

    char name [MAX_NAME_LEN + 1];
    name[6] = '\0';
    while (offset < SECTOR_SIZE)
    {
	memcpy(name, nodes+offset, 6);
	printf("cmp %s %s\n", name, node->name);
	if (strncmp(name, node->name, 6) == 0)
	    break;
	offset += 32;

    }
    if (offset == SECTOR_SIZE)
	return -EFAULT;
    node->size = new_size;
    memcpy( &(nodes[offset+8]), &(node->sectors[0]), new_sectors_count);
    memcpy( &(nodes[offset+6]), &(node->size), 2);
    fs_update_nodes();
    fs_update_map();

    return 0;
}

int fs_count_free_blocks()
{
    int i, free_block_count;
    
    free_block_count = 0;
    for (i=0; i<MAX_MAP_ENTRIES; i++)  {
        if (map[i] == 0x0) free_block_count++;
    }
    
    return free_block_count;
}

////////////////////////////
//    fuse operations     //
////////////////////////////

void* fs_init(struct fuse_conn_info *conn) 
{
    printf("Loading file system map ...\n");
    device_read_sector(map, 1);
    device_read_sector(nodes, 2); 
    return NULL;
}

int fs_getattr(const char *path, struct stat *statbuf)
{
    printf("%s: %s\n", __func__, path);

    int path_len = strlen(path);

    statbuf->st_uid = 0;
    statbuf->st_gid = 0;
    if ( (path_len == 1) && path[0] == '/') {
        statbuf->st_mode = S_IFDIR | S_IRWXU | S_IRWXG | S_IRWXO;
        statbuf->st_nlink = 1;
        statbuf->st_ino = 0;
        statbuf->st_size = SECTOR_SIZE;
        statbuf->st_blksize = SECTOR_SIZE;
        statbuf->st_blocks = 1;
        return 0;
    }

    node_t* n = find_node_by_name(path);
    puts("OK");
    statbuf->st_blksize = SECTOR_SIZE;
    if (n->size == -1)
    {
	puts("file not found");
	return -ENOENT;
    } else {
	if (n->size == -2)	// if dir
	{
	    statbuf->st_mode = S_IFDIR | 0777;
	    statbuf->st_nlink = 3;
	    return 0;
	} else {
	    statbuf->st_mode = S_IFREG | 0666;
	    statbuf->st_nlink = 1;
	    statbuf->st_size = n->size;
	    return 0;
	}
    }
}

int fs_mknod(const char *pth, mode_t mode, dev_t dev)
{
    printf("%s: %s\n", __func__, pth);
    if( S_ISREG(mode) == 0 )
	return -EPERM;

    char *path = (char*)malloc(strlen(pth)+1);
    strncpy(path, pth, strlen(pth)+1);

    int i;
    int slash;
    node_t* parent;
    char* file_name;
    for (i = 0; i < strlen(path); ++i)
	if (path[i] == '/')
	    slash = i;
    if (slash == 0) 
    {
	parent = find_node_by_name("/");
	parent->size = -2;
    } else {
	path[slash] = '\0';
	parent = find_node_by_name(path);
    }
    if (parent->size != -2)
	return -ENOENT;
    file_name = path + slash + 1;

    int node_offset = get_new_node_offset();
    if (node_offset == -1)
	return EFAULT;
    memcpy(nodes + node_offset, file_name, 6);
    memset(nodes + node_offset + 6, 0, 26);
    fs_update_nodes();

    if ( -1 == write_node_info_to_parent_sector
	 (file_name, parent->sectors[0], node_offset / 32))
	return EFAULT;
    return 0;
}

int fs_mkdir(const char *pth, mode_t mode)
{
    printf("%s: %s\n", __func__, pth);

    char *path = (char*)malloc(strlen(pth)+1);
    strncpy(path, pth, strlen(pth)+1);

    int i;
    int slash;
    node_t* parent;
    char* file_name;
    for (i = 0; i < strlen(path); ++i)
	if (path[i] == '/')
	    slash = i;
    file_name = path + slash + 1;
    if (slash == 0) 
    {
	parent = find_node_by_name("/");
	parent->size = -2;
    } else {
	path[slash] = '\0';
	parent = find_node_by_name(path);
    }
    if ( (2 + parent->size) != 0)
	return -ENOENT;

    int node_offset = get_new_node_offset();
    if (node_offset == -1)
	return EFAULT;
    memcpy(nodes + node_offset, file_name, 6);
    *((int16_t*) (nodes + node_offset + 6)) = (int16_t) -2; 
    memset(nodes + node_offset + 8, 0, 26);

    int sector = fs_get_free_sector();
    if (sector == -1) 
	return -ENOSPC;
    map[sector] = 0xFF;
    nodes[node_offset + 8] = sector;

    char empty_sector[SECTOR_SIZE];
    memset(empty_sector, 0, sector);
    device_flush();

    fs_update_nodes();
    fs_update_map();

    if ( -1 == write_node_info_to_parent_sector
	 (file_name, parent->sectors[0], node_offset / 32))
	return EFAULT;

    return 0;
}

int fs_unlink(const char *path)
{
    printf("%s: %s\n", __func__, path);
    node_t* node = find_node_by_name(path);
    node_t* parent_node = find_parent_node(path);

    int i;
    for (i = 0; node->sectors[i] != 0; i++)
	map[ node->sectors[i] ] = 0x00;

    file_info_t* parent_fi = get_file_info_by_node(parent_node);
    unsigned char* par_data = parent_fi->f_data;
	
    for (i = 0; i < SECTOR_SIZE; i++)
	if (strncmp( (char*)(par_data + i), node->name, 6) == 0)
	    break;

    for ( i += 8; i < SECTOR_SIZE; i++)
	par_data[ i - 8 ] = par_data[i];

    fs_update_nodes();
    fs_update_map();

    device_write_sector(par_data, parent_node->sectors[0]);
    device_flush();

    return 0;
}

int fs_rmdir(const char *path)
{
    printf("%s: %s\n", __func__, path);

    node_t* node = find_node_by_name(path);

    file_info_t* dir_info = get_file_info_by_node(node);
    if ( dir_info->f_data[0] != 0)
	return -ENOTEMPTY;

    int i;
    for (i = 0; node->sectors[i] != 0; i++)
	map[ node->sectors[i] ] = 0x00;

    node_t* parent_node = find_parent_node(path);
    file_info_t* parent_fi = get_file_info_by_node(parent_node);
    unsigned char* par_data = parent_fi->f_data;
	
    for (i = 0; i < SECTOR_SIZE; i++)
	if (strncmp( (char*)(par_data + i), node->name, 6) == 0)
	    break;

    for ( i += 8; i < SECTOR_SIZE; i++)
	par_data[ i - 8 ] = par_data[i];

    fs_update_nodes();
    fs_update_map();

    device_write_sector(par_data, parent_node->sectors[0]);
    device_flush();
    return 0;
}

int fs_utimens(const char *path, const struct timespec tv[2])
{
    /**
     *	Yes, it should be like this
     **/
    return 0;
}

int fs_truncate(const char *pth, off_t new_size)
{
    printf("%s: %s\n", __func__, pth);

    char *path = (char*)malloc(strlen(pth)+1);
    strncpy(path, pth, strlen(pth)+1);

    node_t* node = find_node_by_name(path);
    if (new_size <= node->size)
	return -EFBIG;
    int sectors_count = fs_sectors_count(new_size);
    int i, slash;
    for ( i = sectors_count; node->sectors[i] != 0; i++)
    {
	map[ node->sectors[i] ] = 0x00;
	node->sectors[i] = 0;
    }
    node->size = new_size;

    file_info_t* parent_fi = get_file_info_by_node(find_parent_node(path));
	
    char* file_name;
    for (i = 0; i < strlen(path); ++i)
	if (path[i] == '/')
	    slash = i;
    file_name = path + slash + 1;

    for (int i = 0; i < SECTOR_SIZE; i += 8)
	if (strncpy( (char*)&(parent_fi->f_data[i]), file_name, 6) == 0)
	    break;

    int node_number = parent_fi->f_data[i + 7];

    *((int16_t*)(&(nodes[ 32*node_number + 6 ]))) = (int16_t) new_size;
    memcpy( &(nodes[32*node_number + 8]), node->sectors, 24);

    fs_update_nodes();
    fs_update_map();
    return 0;
}

int fs_open(const char *path, struct fuse_file_info *fileInfo)
{
    printf("%s: %s\n", __func__, path);
    node_t *node = find_node_by_name(path);
    file_info_t* fi = get_file_info_by_node(node);
    fileInfo->fh = (uint64_t)fi;

    return 0;
}

int fs_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fileInfo)
{
    printf("%s: %s\n", __func__, path);
    file_info_t* fi = (file_info_t*) fileInfo->fh;

    if (offset >= fi->f_node->size)
	return 0;

    int bytes_to_read = size;
    if (offset + size > fi->f_node->size)
	bytes_to_read = fi->f_node->size - offset;

    memcpy(buf, &fi->f_data[offset], bytes_to_read);

    return bytes_to_read;
}

int fs_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fileInfo)
{
    printf("%s: %s\n", __func__, path);
    file_info_t* fi = (file_info_t*) fileInfo->fh;
    fi->need_update = 1;

    printf("%s, after FNBN name: %s\n", __func__, fi->f_node->name);
    if (offset + size > fi->f_node->size)
    {
	if (offset + size > MAX_FILE_SIZE)
	    return EFBIG;

	int new_size = (offset + size);
	int ret = fs_grow_file(fi, new_size);
	if (ret < 0)
	    return ret;
	fi->f_node->size = new_size;
    }

    memcpy(&(fi->f_data[offset]), buf, size);

    return size;
}

int fs_statfs(const char *path, struct statvfs *statInfo)
{
    printf("%s: %s\n", __func__, path);
    int free_blocks = fs_count_free_blocks();

    statInfo->f_bsize = SECTOR_SIZE;  
    statInfo->f_frsize = SECTOR_SIZE; 
    statInfo->f_blocks = SECTOR_SIZE;
    statInfo->f_bfree = free_blocks;  
    statInfo->f_bavail = free_blocks;  
    statInfo->f_fsid = 0;                  
    statInfo->f_flag = 0;                  
    statInfo->f_namemax = MAX_NAME_LEN;    
    return 0;
}

int fs_flush(const char *path, struct fuse_file_info *fileInfo)
{
    printf("%s: %s\n", __func__, path);
    file_info_t* fi = (file_info_t*) fileInfo->fh;
    if (fi->need_update == 0)
	return 0;
    unsigned char* ptr_data = fi->f_data;
    for (int i = 0; fi->f_node->sectors[i] != 0; ++i)
    {
	device_write_sector(ptr_data, fi->f_node->sectors[i]);
	ptr_data += SECTOR_SIZE;
    }

    device_flush();

    return 0;
}

int fs_opendir(const char *path, struct fuse_file_info *fileInfo)
{
    printf("%s: %s\n", __func__, path);
    node_t* n = find_node_by_name(path);
    if (n->size == -1) 
	return -ENOENT;
    if (n->size != -2)
	return -ENOENT;
    return 0;
}

int fs_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fileInfo)
{
    printf("%s: %s\n", __func__, path);
    node_t* n = find_node_by_name(path);
    if (n->size == -1)
    {
	puts("Error while readdir");
	return -ENOENT;
    } else {
	if (n->size == -2)
	{
	    filler(buf, ".", NULL, 0);
	    filler(buf, "..", NULL, 0);
	    file_info_t* fi = get_file_info_by_node(n);
	    int name_index = 0;
	    char name[MAX_NAME_LEN+1] = {0};
	    while (fi->f_data[name_index] != 0 && name_index < SECTOR_SIZE)
	    {
		memcpy (name, &(fi->f_data[name_index]), 6);
		filler(buf, name, NULL, 0);
		name_index += 8;
	    }
	    return 0;
	}
	return -ENOENT;
    }
}

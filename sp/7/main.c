#include "fs.h"

#include <fuse.h>
#include <stdio.h>

struct fuse_operations fs_oper =
{
    .getattr = fs_getattr,
    .getdir = NULL,
    .mknod = fs_mknod,
    .mkdir = fs_mkdir,
    .unlink = fs_unlink,
    .rmdir = fs_rmdir,
    .truncate = fs_truncate,
    .open = fs_open,
    .read = fs_read,
    .write = fs_write,
    .statfs = fs_statfs,
    .flush = fs_flush,
    .opendir = fs_opendir,
    .readdir = fs_readdir,
    .init = fs_init,
    .utimens = fs_utimens
};

int main(int argc, char *argv[]) {
    int i, fuse_stat;

    printf("mounting file system...\n");
	
    for(i = 1; i < argc && (argv[i][0] == '-'); i++) 
	;

    if (!device_open(realpath(argv[i], NULL)) )
    {
	printf("Cannot open device file %s\n", argv[i]);
	return 1;
    }

    for(; i < argc; i++)
    	argv[i] = argv[i+1];
    
    argc--;

    fuse_stat = fuse_main(argc, argv, &fs_oper, NULL);

    device_close();
    
    printf("fuse_main returned %d\n", fuse_stat);

    return fuse_stat;
}



#ifndef fs_h
#define fs_h

#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <fuse.h>
#include <libgen.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdint.h>
#include "device.h"

#define MAX_NAME_LEN		    6
#define MAX_SECTORS_PER_FILE	24
#define MAX_FILES_IN_DIR		64
#define MAX_FILE_SIZE           (MAX_SECTORS_PER_FILE * SECTOR_SIZE)
#define NODE_LEN				32
#define MAX_MAP_ENTRIES         256
#define MAX_NODES				16

typedef struct node_t {
    char name[MAX_NAME_LEN];
    int16_t size;
    unsigned char sectors[MAX_SECTORS_PER_FILE];
} node_t;

typedef struct file_info_t {
    struct node_t *f_node;
    unsigned char f_data[MAX_FILE_SIZE];
    int need_update;
} file_info_t;

int fs_getattr(const char *path, struct stat *statbuf);
int fs_mknod(const char *path, mode_t mode, dev_t dev);
int fs_mkdir(const char *path, mode_t mode);
int fs_unlink(const char *path);
int fs_rmdir(const char *path);
int fs_truncate(const char *path, off_t newSize);
int fs_open(const char *path, struct fuse_file_info *fileInfo);
int fs_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fileInfo);
int fs_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fileInfo);
int fs_statfs(const char *path, struct statvfs *statInfo);
int fs_flush(const char *path, struct fuse_file_info *fileInfo);
int fs_opendir(const char *path, struct fuse_file_info *fileInfo);
int fs_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fileInfo);
void* fs_init(struct fuse_conn_info *conn);
int fs_utimens(const char *, const struct timespec tv[2]);
#endif


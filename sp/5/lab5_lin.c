#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dlfcn.h>
#include <pthread.h>
#include <errno.h>
#include "lab5lib.h"

typedef void (*fun_t)(void*);

#define TWENTY_MB 2097

void* lib_desc;

void  (*asio_read)  (pthread_mutex_t*);
void* (*asio_write) (void*);

void connect_lib() 
{
    lib_desc = dlopen("./asio_lin.so", RTLD_LAZY);
    if (!lib_desc)
    {
	puts(dlerror());
	exit(EXIT_FAILURE);
    }
    asio_write = dlsym(lib_desc, "asio_write");
    asio_read = (fun_t)dlsym(lib_desc, "asio_read");

}

void create_thread()
{
    pthread_mutex_t mutex;
    pthread_mutex_init(&mutex, NULL);

    pthread_t tid;
    if (pthread_create(
	    &tid,
	    NULL,
	    asio_write,
	    (void*) &mutex) != 0)
    {
	fputs("Creating thread error", stderr);
	exit(EXIT_FAILURE);
    }

    asio_read (&mutex);
    pthread_join(tid, NULL);
    pthread_mutex_destroy(&mutex);
}

void disconnect_lib()
{
    dlclose(lib_desc);
}

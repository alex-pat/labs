#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

#define TWENTY_MB 20971520

static const char* input_paths[] = 
{
    "out/1",
    "out/2",
    "out/3",
    "out/4",
    "out/5"
};

static const char* output_path = "out/out";

DWORD bufsize = 0;
char buffer[TWENTY_MB];

void asio_read  (CRITICAL_SECTION* critical_section);
void asio_write (LPVOID critical_section);

void asio_read(CRITICAL_SECTION* critical_section)
{
    HANDLE file;
    for (int i = 0; i < 5; i++)
    {
	file = CreateFileA(input_paths[i],
			   GENERIC_READ,
			   0,
			   NULL,
			   OPEN_EXISTING,
			   FILE_FLAG_OVERLAPPED,
			   NULL);	
	if (file == INVALID_HANDLE_VALUE)
	{
	    fputs("Opening file error", stderr);
	    exit(EXIT_FAILURE);
	}
	
	OVERLAPPED overlapped = {0};
	EnterCriticalSection(critical_section);
	if (ReadFile(file,
		     buffer,
		     TWENTY_MB,
		     &bufsize,
		     (LPOVERLAPPED)&overlapped) != 0)
	{
	    fputs("Reading file error", stderr);
	    exit(GetLastError());
	}
	printf("File %s is reading... ", input_paths[i]);
	
	if (GetOverlappedResult(file,
				&overlapped,
				&bufsize, TRUE) == 0)
	{
	    fputs("Reading file error", stderr);
	    exit(EXIT_FAILURE);
	}
	puts("Success");
	LeaveCriticalSection(critical_section);
	CloseHandle(file);
	Sleep(1000);
	
    }

}

void asio_write(LPVOID p_critical_section)
{
    CRITICAL_SECTION* critical_section = (CRITICAL_SECTION*) p_critical_section;
        
    HANDLE file = CreateFileA(output_path,
			      FILE_ALL_ACCESS,
			      0,
			      NULL,
			      CREATE_ALWAYS,
			      FILE_FLAG_OVERLAPPED,
			      NULL);
    if (file == INVALID_HANDLE_VALUE)
    {
	fputs("Opening output file error", stderr);
	exit(EXIT_FAILURE);
    }

    while (bufsize == 0)
	;
    Sleep(500);
    for (int i = 0; i < 5; i++)
    {
	EnterCriticalSection(critical_section);
	DWORD file_pointer = SetFilePointer(file,
					    0,
					    NULL,
					    FILE_END);
	OVERLAPPED overlapped = {0};
	overlapped.Offset = file_pointer;
	if ( ! WriteFile(file,
			 buffer,
			 bufsize,
			 NULL,
			 &overlapped)
	     && GetLastError() != ERROR_IO_PENDING)
	{
	    fputs("Writing to file error", stderr);
	    exit(EXIT_FAILURE);
	}
	printf("File %s is reading... ", output_path);

	if (GetOverlappedResult(file,
				&overlapped,
				&bufsize, TRUE) == 0)
	{
	    fputs("Reading file error", stderr);
	    exit(EXIT_FAILURE);
	}
	puts("Success");
	LeaveCriticalSection(critical_section);

	Sleep(1000);

    }
    CloseHandle(file);
    return;
}

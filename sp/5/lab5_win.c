#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include "lab5lib.h"

#define TWENTY_MB 20971520

HMODULE h_dinamic_lib;

void (*asio_read) (CRITICAL_SECTION* critical_section);
void (*asio_write)(LPVOID p_critical_section);
DWORD WINAPI run_write(LPVOID p_critical_section);

void connect_lib()
{
    h_dinamic_lib = LoadLibrary("asio_win.dll");

    if (!h_dinamic_lib)
    {
	fputs("Library loading error", stderr);
	exit(EXIT_FAILURE);
    }

    asio_read  = (void (*)() ) GetProcAddress(h_dinamic_lib, "asio_read");
}

void create_thread()
{
    CRITICAL_SECTION critical_section;
    InitializeCriticalSection(&critical_section);
    
    HANDLE writer = CreateThread(NULL,
    				 0,
    				 run_write,
    				 &critical_section,
    				 0,
    				 NULL);
    if (writer == NULL)
    {
    	fputs("Creating thread error", stderr);
    	exit(GetLastError());
    }
    
    (*asio_read) (&critical_section);
    WaitForSingleObject(writer, INFINITE);
    DeleteCriticalSection(&critical_section);
}

void disconnect_lib()
{
    FreeLibrary(h_dinamic_lib);
}

DWORD WINAPI run_write(LPVOID p_critical_section)
{
    HMODULE h_din_lib = LoadLibrary("asio_win.dll");

    if (!h_din_lib)
    {
    	fputs("Library loading error", stderr);
    	exit(EXIT_FAILURE);
    }

    asio_write = (void (*)() ) GetProcAddress(h_dinamic_lib,
					      "asio_write");
    asio_write(p_critical_section);
    
    FreeLibrary(h_din_lib);
    return 0;
}

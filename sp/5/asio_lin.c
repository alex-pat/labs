#define _XOPEN_SOURCE 600
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <aio.h>
#include <pthread.h>

#define TWENTY_MB 20971520

static const char* input_paths[] = 
{
    "out/1",
    "out/2",
    "out/3",
    "out/4",
    "out/5"
};

static const char* output_path = "out/out";

size_t bufsize = 0;
char buffer[TWENTY_MB];

void  asio_read (pthread_mutex_t*);
void* asio_write (void*);

void asio_read( pthread_mutex_t* mutex )
{
    int fd, ret;
    struct aiocb aio_info = {0};
    aio_info.aio_buf = &buffer;
    aio_info.aio_nbytes = TWENTY_MB;
    for (int i = 0; i < 5; ++i)
    {
	fd = open(input_paths[i], O_RDONLY);
	if (fd < 0) 
	{
	    fprintf(stderr, "Opening file %s error", input_paths[i]);
	    exit(EXIT_FAILURE);
	}
	pthread_mutex_lock(mutex);
	aio_info.aio_fildes = fd;
	ret = aio_read( &aio_info );
	if (ret < 0)
	{
	    perror("aio_read");
	    close(fd);
	    exit(EXIT_FAILURE);
	}
	printf("File %s is reading.", input_paths[i]);
	while (aio_error( &aio_info ) == EINPROGRESS)
	{
	    putchar('.');
	    fflush(stdout);
	    usleep(100000);			
	}
	if ( (ret = aio_return(&aio_info)) < 0)
	{
	    perror("aio_read");
	    close(fd);
	    exit(EXIT_FAILURE);
	}

	puts("Success");
	bufsize = ret;
	pthread_mutex_unlock(mutex);
	close(fd);

	usleep(500000);
    }
	
}

void* asio_write (void* mutex)
{
    int fd, ret;
    struct aiocb aio_info = {0};
    aio_info.aio_buf = buffer;

    fd = open(output_path, O_CREAT | O_WRONLY);
    if (fd < 0) 
    {
	fputs("Opening output file error", stderr);
	exit(EXIT_FAILURE);
    }
    aio_info.aio_fildes = fd;

    while (bufsize == 0)
	;
    usleep(500000);
    for (int i = 0; i < 5; ++i)
    {
	pthread_mutex_lock( (pthread_mutex_t*) mutex );
	aio_info.aio_nbytes = bufsize;
	ret = aio_write(&aio_info);
	if (ret < 0)
	{
	    perror("aio_write");
	    close(fd);
	    exit(EXIT_FAILURE);
	}
	printf("Writing to file %s.", output_path);
	while (aio_error( &aio_info ) == EINPROGRESS)
	{
	    putchar('.');
	    fflush(stdout);
	    usleep(100000);
	}
	if ( (ret = aio_return( &aio_info )) < 0)
	{
	    perror("aio_write");
	    close(fd);
	    exit(EXIT_FAILURE);
	}

	puts("Success");
	pthread_mutex_unlock(mutex);
	aio_info.aio_offset += ret;

	usleep(500000);
    }

    close(fd);

    return NULL;
}

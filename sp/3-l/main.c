#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<unistd.h>
#include<signal.h>
#include<sys/types.h>
#include<fcntl.h>

#define MAXBUF 256
char charbuf[MAXBUF];

int pipefd[2];
pid_t pid;
int parent;
struct sigaction sa;
int i;

void sa_receive(int sig)
{
    int asize;
    asize = read(pipefd[0], charbuf, MAXBUF);
    if (asize == -1)
    {
	perror("read");
    }
    printf("server   : %s \n", charbuf);
    fflush(stdout);
    kill(pid, SIGUSR2);        
}

void sa_respond(int sig)
{
    printf("client OK> ");
    fflush(stdout);    
}

void sa_child_death(int sig)
{
    puts("server shutdown");
    close(pipefd[1]);
    exit(EXIT_SUCCESS);
}

int main(int argc, char * argv[])
{  
    sa.sa_handler = sa_receive;
    sigaction(SIGUSR1, &sa, NULL);
    
    sa.sa_handler = sa_respond;

    sigaction(SIGUSR2, &sa, NULL);
    
    sa.sa_handler = sa_child_death;

    sigaction(SIGCHLD, &sa, NULL);    
    
    i = pipe(pipefd);    
    if (i == -1)
    {
	perror("pipe");
	return EXIT_FAILURE;
    }

    pid = fork();
    if (pid == -1)
    {
	perror("fork");
	return EXIT_FAILURE;
    }
      
    if (pid == 0)
    {
	close(pipefd[0]);
	pid = getppid();    
	parent = 0;

        printf(">");	
	while(1)
	{
	    scanf("%s", charbuf);
	    if (strcmp(charbuf, "exit") == 0) 
	    {
		puts("client shutdown");
		close(pipefd[1]);
		exit(EXIT_SUCCESS);
	    }
	    write(pipefd[1], charbuf, strlen(charbuf) + 1);
	    kill(pid, SIGUSR1);
	    pause();
	}	
    }
    else
    {
	close(pipefd[1]);
	parent = 1;
	while(1)
	{
	    pause();
	}
    }
    
    return 0;
}
